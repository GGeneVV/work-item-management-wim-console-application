﻿using System;
using System.Collections.Generic;
using System.Text;
using WIM.Commands.Contracts;
using WIM.Core.Contracts;

namespace WIM.Commands.Abstracts
{
    public abstract class Command : ICommand
    {
        public Command(IList<string> commandParameters)
        {
            this.CommandParameters = new List<string>(commandParameters);
        }
                
        protected IList<string> CommandParameters
        {
            get;
        }   

        protected IDatabase Database
        {
            get
            {
                return Core.Database.Instance;
            }
        }

        protected IFactory Factory
        {
            get
            {
                return Core.Factory.Instance;
            }
        }

        public abstract string Execute();
    }
}
