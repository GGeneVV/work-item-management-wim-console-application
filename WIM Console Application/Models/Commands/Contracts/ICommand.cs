﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WIM.Commands.Contracts
{
    public interface ICommand
    {
        string Execute();
    }
}
