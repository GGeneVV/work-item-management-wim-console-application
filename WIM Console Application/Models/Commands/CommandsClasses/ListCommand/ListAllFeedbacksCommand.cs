﻿using System;
using System.Collections.Generic;
using System.Text;
using WIM.Commands.Abstracts;

namespace WIM.Commands.CommandClasses
{
    public class ListAllFeedbacksCommand : Command
    {
        public ListAllFeedbacksCommand(IList<string> commandParameters) : base(commandParameters)
        {
        }
        public override string Execute()
        {
            StringBuilder sb = new StringBuilder();

            if (this.Database.Feedback.Count == 0)
            {
                return "There is no feedbacks to show!";
            }
            else
            {
                foreach (var item in this.Database.Feedback)
                {
                    sb.Append(item.ToString());
                }
            }

            return sb.ToString();
        }
    }
}
