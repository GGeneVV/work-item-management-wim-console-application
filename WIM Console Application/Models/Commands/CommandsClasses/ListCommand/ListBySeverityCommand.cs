﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WIM.Commands.Abstracts;
using WIM.Models.Enums;

namespace WIM.Commands.CommandClasses
{
    public class ListBySeverityCommand : Command
    {
        public ListBySeverityCommand(IList<string> commandParameters) : base(commandParameters)
        {
        }

        public override string Execute()
        {
            StringBuilder sb = new StringBuilder();
            if (this.Database.Bug.Count == 0)
            {
                sb.AppendLine("There is no bugs to show!");
            }

            var isBugSeverity = Enum.TryParse<Severity>(CommandParameters[0], true, out Severity severity);

            if (!isBugSeverity)
            { 
                sb.AppendLine($"{CommandParameters[0]} is not a valid bug severity!");
            }
            else
            {
                foreach (var item in this.Database.Bug.Where(x => x.Severity == severity))
                {
                    sb.AppendLine(item.ToString());
                }
            }

            return sb.ToString();
        }
    }
}
