﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WIM.Commands.Abstracts;
using WIM.Core.Contracts;
using WIM.Models.Contracts;
using WIM.Models.Units;

namespace WIM.Commands.CommandClasses
{
    public class ListByTitleCommand : Command
    {
        public ListByTitleCommand(IList<string> commandParameters) : base(commandParameters)
        {
        }

        public override string Execute()
        {
            var parameter = this.CommandParameters[0];
            StringBuilder sb = new StringBuilder();

            switch (parameter)
            {
                case "bug":
                    var bugList = new List<IBug>();
                    if (this.Database.Bug.Count == 0)
                    {
                        sb.AppendLine("There is no bugs to show!");
                    }
                    else
                    {
                        bugList = this.Database.Bug.OrderBy(t => t.Title).ToList();
                    }
                    foreach (var item in bugList)
                    {
                        sb.AppendLine(item.ToString());
                    }
                    break;

                case "story":
                    var storyList = new List<IStory>();
                    if (this.Database.Story.Count == 0)
                    {
                        sb.AppendLine("There is no stories to show!");
                    }
                    else
                    {
                        storyList = this.Database.Story.OrderBy(t => t.Title).ToList();
                    }
                    foreach (var item in storyList)
                    {
                        sb.AppendLine(item.ToString());
                    }
                    break;

                case "feedback":
                    var feedbackList = new List<IFeedback>();
                    if (this.Database.Feedback.Count == 0)
                    {
                        sb.AppendLine("There is no feedbacks to show!");
                    }
                    else
                    {
                        feedbackList = this.Database.Feedback.OrderBy(t => t.Title).ToList();
                    }
                    foreach (var item in feedbackList)
                    {
                        sb.AppendLine(item.ToString());
                    }
                    break;

                case "all":
                    var workItems = new List<IWorkItem>();

                    Database.Bug.ForEach(x => workItems.Add(x));
                    Database.Story.ForEach(x => workItems.Add(x));
                    Database.Feedback.ForEach(x => workItems.Add(x));
                    workItems = workItems.OrderBy(t => t.Title).ToList();

                    foreach (var item in workItems)
                    {
                        sb.AppendLine(item.ToString());
                    }
                    break;
            }
            return sb.ToString();
        }
    }
}
