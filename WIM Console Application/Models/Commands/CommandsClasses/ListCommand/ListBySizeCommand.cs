﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WIM.Commands.Abstracts;
using WIM.Models.Enums;

namespace WIM.Commands.CommandClasses
{
    public class ListBySizeCommand : Command
    {
        public ListBySizeCommand(IList<string> commandParameters) : base(commandParameters)
        { 
        }

        public override string Execute()
        {
            StringBuilder sb = new StringBuilder();
            if (this.Database.Story.Count == 0)
            {
                sb.AppendLine("There is no stories to show!");
            }

            var isStoryStatus = Enum.TryParse<StorySize>(CommandParameters[0], true, out StorySize storySize);
           
            if (!isStoryStatus)
            {
                sb.AppendLine($"{CommandParameters[0]} is not a valid story size!");
            }
            else
            {
                foreach (var item in this.Database.Story.Where(x => x.Size == storySize))
                {
                    sb.AppendLine(item.ToString());
                }
            }

            return sb.ToString();
        }
    }
}
