﻿
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WIM.Commands.Abstracts;
using WIM.Models.Enums;
using WIM.Models.Units;

namespace WIM.Commands.CommandClasses
{
    public class ListByRatingCommand : Command
    {
        public ListByRatingCommand(IList<string> commandParameters) : base(commandParameters)
        {
        }

        public override string Execute()
        {
           StringBuilder sb = new StringBuilder();
            if (this.Database.Feedback.Count == 0)
            {
                sb.AppendLine("There is no feedback to show!");
                return sb.ToString();
            }
     
            
            else
            {
                foreach (var item in this.Database.Feedback.OrderBy(x => x.Rating))
                {
                    sb.AppendLine(item.ToString());
                }

            }
            return sb.ToString();
        }
    }
}
