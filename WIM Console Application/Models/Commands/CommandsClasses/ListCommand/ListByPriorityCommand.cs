﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WIM.Commands.Abstracts;
using WIM.Models.Enums;

namespace WIM.Commands.CommandClasses
{
    public class ListByPriorityCommand : Command
    {
        public ListByPriorityCommand(IList<string> commandParameters) : base(commandParameters)
        {
        }

        public override string Execute()
        {
            StringBuilder sb = new StringBuilder();
            if (this.Database.Story.Count == 0 && this.Database.Bug.Count == 0)
            {
                sb.AppendLine("There is no workitems to show!");
            }
            else if (!Enum.TryParse<Priority>(CommandParameters[0], true, out Priority priority))
            {
                sb.AppendLine("There is no workitems with this priority.!");
            }
            else
            {
                foreach (var item in this.Database.Bug.Where(x => x.Priority == priority))
                {
                    sb.AppendLine(item.ToString());
                }
                foreach (var item in this.Database.Story.Where(x => x.Priority == priority))
                {
                    sb.AppendLine(item.ToString());
                }
            }
            return sb.ToString();
        }
    }
}
