﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WIM.Commands.Abstracts;

namespace WIM.Commands.CommandClasses
{
    class ShowTeamsActivityCommand :Command
    {        public ShowTeamsActivityCommand(IList<string> commandParameters) : base(commandParameters)
        {
        }
        public override string Execute()
        {
            StringBuilder sb = new StringBuilder();
            var teamToShowActivity = this.Database.Teams.First(x => x.Name == CommandParameters[0]);

            if (teamToShowActivity==null)
            {
                throw new Exception("No such team");
            }
            foreach (var item in teamToShowActivity.ActivityHistories)
            {
                sb.AppendLine(item.ToString());
            }
            return sb.ToString();
        }
    }
}
