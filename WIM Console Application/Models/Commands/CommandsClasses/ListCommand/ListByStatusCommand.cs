﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WIM.Commands.Abstracts;
using WIM.Models.Enums;

namespace WIM.Commands.CommandClasses
{
    public class ListByStatusCommand : Command
    {
        public ListByStatusCommand(IList<string> commandParameters) : base(commandParameters)
        {
        }

        public override string Execute()
        {
            var isStoryStatus = Enum.TryParse<StoryStatus>(CommandParameters[0], true, out StoryStatus storyStatus);
            var isBugStatus = Enum.TryParse<BugStatus>(CommandParameters[0], true, out BugStatus bugStatus);
            var isFeedbackStatus = Enum.TryParse<FeedbackStatus>(CommandParameters[0], true, out FeedbackStatus feedbackStatus);

            StringBuilder sb = new StringBuilder();

            if (!isStoryStatus && !isBugStatus && !isFeedbackStatus)
            {
                sb.AppendLine($"{CommandParameters[0]} is not a valid status!");
                return sb.ToString();
            }

            if (isBugStatus)
            {
                foreach (var item in this.Database.Bug.Where(x => x.BugStatus == bugStatus))
                {
                    sb.AppendLine(item.ToString());
                }
            }

            else if (isStoryStatus && !isFeedbackStatus)
            {
                foreach (var item in this.Database.Story.Where(x => x.StoryStatus == storyStatus))
                {
                    sb.AppendLine(item.ToString());
                }
            }

            else if (isFeedbackStatus && !isStoryStatus)
            {
                foreach (var item in this.Database.Feedback.Where(x => x.FeedbackStatus == feedbackStatus))
                {
                    sb.AppendLine(item.ToString());
                }
            }

            else if (isFeedbackStatus && isStoryStatus)
            {
                foreach (var item in this.Database.Feedback.Where(x => x.FeedbackStatus == feedbackStatus))
                {
                    sb.AppendLine(item.ToString());
                }

                foreach (var item in this.Database.Story.Where(x => x.StoryStatus == storyStatus))
                {
                    sb.AppendLine(item.ToString());
                }
            }

            if (sb.Length == 0)
            {
                sb.AppendLine($"There is no element with status {CommandParameters[0]}");
            }
            return sb.ToString();
        }
    }
}
