﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WIM.Commands.Abstracts;

namespace WIM.Commands.CommandClasses
{
    class ShowAllTeamMembersCommand : Command
    {
        public ShowAllTeamMembersCommand(IList<string> commandParameters) : base(commandParameters)
        {
        }
        public override string Execute()
        {
            StringBuilder sb = new StringBuilder();
            var teamToListMembers = this.Database.Teams.First(x => x.Name == CommandParameters[0]);

            foreach (var item in teamToListMembers.Members)
            {
                sb.AppendLine(item.ToString());
            }
            return sb.ToString();
        }
    }
}
