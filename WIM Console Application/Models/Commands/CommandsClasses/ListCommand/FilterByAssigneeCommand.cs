﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WIM.Commands.Abstracts;

namespace WIM.Commands.CommandClasses
{
    public class FilterByAssigneeCommand : Command
    {

        public FilterByAssigneeCommand(IList<string> commandParameters) : base(commandParameters)
        {
        }
        public override string Execute()
        {
            StringBuilder sb = new StringBuilder();
            var personName = CommandParameters[0];
          //  var membern = this.Database.Members.First(x => x.Name == personName);
            
            if (!this.Database.Members.Any(x => x.Name == personName))
            {
                sb.AppendLine($"There is no member with name {personName}");
            }
            else
            {
                sb.AppendLine($"{personName}:");

                foreach (var item in this.Database.Bug.Where(x => x.Assignee.Name == personName))
                {
                    sb.AppendLine(item.ToString());
                    
                }
                foreach (var item in this.Database.Story.Where(x => x.Assignee.Name == personName))
                {
                    sb.AppendLine(item.ToString());
                }
            }

            return sb.ToString();
        }
    }
}

