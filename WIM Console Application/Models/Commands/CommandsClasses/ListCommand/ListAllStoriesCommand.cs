﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Metadata.Ecma335;
using System.Runtime.InteropServices;
using System.Text;
using WIM.Commands.Abstracts;

namespace WIM.Commands.CommandClasses
{
    public class ListAllStoriesCommand : Command
    {
        public ListAllStoriesCommand(IList<string> commandParameters) : base(commandParameters)
        {
        }
        public override string Execute()
        {
            StringBuilder sb = new StringBuilder();
            if (!(this.Database.Story.Count > 0))
            {
                sb.AppendLine("There is no stories to show!");
            }
            foreach (var item in this.Database.Story)
            {
                sb.AppendLine(item.ToString());
            }

            return sb.ToString();
        }
    }
}
