﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WIM.Commands.Abstracts;

namespace WIM.Commands.CommandClasses
{
    public class ListAllBugsCommand : Command
    {
        public ListAllBugsCommand(IList<string> commandParameters) : base(commandParameters)
        {
        }

        public override string Execute()
        {
            StringBuilder sb = new StringBuilder();
            if (this.Database.Bug.Count < 1)
            {
                sb.AppendLine("There is no bugs to show!");
            }
            else
            {
                foreach (var item in this.Database.Bug)
                {
                    sb.AppendLine(item.ToString());
                }
            }
            return sb.ToString();

         
        }
    }
}
