﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WIM.Commands.Abstracts;

namespace WIM.Commands.CommandClasses
{
    class ShowBoardsActivityCommand: Command
    {
        public ShowBoardsActivityCommand(IList<string> commandParameters) : base(commandParameters)
        {
        }
        public override string Execute()
        {
            StringBuilder sb = new StringBuilder();
            
            var boardsToShowActivity = this.Database.Boards.FirstOrDefault(x => x.Name == CommandParameters[0]);

            foreach (var item in boardsToShowActivity.ActivityHistory)
            {
                sb.AppendLine(item.ToString());
            }
            if (sb.Length ==0)
            {
                sb.AppendLine("There is no activity to show!");
            }
            return sb.ToString();
        }
    }
}
