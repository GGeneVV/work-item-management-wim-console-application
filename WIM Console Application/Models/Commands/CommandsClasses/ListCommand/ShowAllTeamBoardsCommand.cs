﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WIM.Commands.Abstracts;

namespace WIM.Commands.CommandClasses
{
    public class ShowAllTeamBoardsCommand :Command
    {
        public ShowAllTeamBoardsCommand(IList<string> commandParameters) : base(commandParameters)
        {
        }
        public override string Execute()
        {           
            StringBuilder sb = new StringBuilder();
            var teamTiListBoards = this.Database.Teams.First(x => x.Name == CommandParameters[0]);

            foreach (var item in teamTiListBoards.Boards)
            {
                sb.AppendLine(item.ToString());
            }
            return sb.ToString();
        }
   
    }
}
