﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WIM.Commands.Abstracts;
using WIM.Models.Abstract;
using WIM.Models.Enums;
using WIM.Models.Units;

namespace WIM.Commands.CommandClasses
{
    public class FilterByStatusAndAssigneeCommand : Command
    {

        public FilterByStatusAndAssigneeCommand(IList<string> commandParameters) : base(commandParameters)
        {
        }
        public override string Execute()
        {
            StringBuilder sb = new StringBuilder();

            var isStoryStatus = Enum.TryParse<StoryStatus>(CommandParameters[0], true, out StoryStatus storyStatus);
            var isBugStatus = Enum.TryParse<BugStatus>(CommandParameters[0], true, out BugStatus bugStatus);    
            var bugSortedBuStatus = new List<Bug>();
            var storySortedBuStatus = new List<Story>();
            var personName = CommandParameters[1];
           
            if (!isStoryStatus && !isBugStatus)
            {
                sb.AppendLine($"{CommandParameters[0]} is not a valid status!");
                return sb.ToString();
            }

            if (!this.Database.Members.Any(x => x.Name == personName))
            {
                sb.AppendLine($"There is no member wiht name {personName}");
                return sb.ToString();
            }

            if (isBugStatus)
            {
                foreach (var item in this.Database.Bug.Where(x => x.BugStatus == bugStatus))
                {
                    bugSortedBuStatus.Add((Bug)item);
                }
            }

            else if (isStoryStatus)
            {
                foreach (var item in this.Database.Story.Where(x => x.StoryStatus == storyStatus))
                {
                    storySortedBuStatus.Add((Story)item);
                }
            }
                        
            foreach (var item in bugSortedBuStatus.Where(x => x.Assignee.Name == personName))
            {
                sb.AppendLine(item.ToString());
            }
            foreach (var item in storySortedBuStatus.Where(x => x.Assignee.Name == personName))
            {
                sb.AppendLine(item.ToString());
            }

            if (sb.Length == 0)
            {
                sb.AppendLine($"There is no element with status {CommandParameters[0]}");
            }

            return sb.ToString();
        }
    }
}

