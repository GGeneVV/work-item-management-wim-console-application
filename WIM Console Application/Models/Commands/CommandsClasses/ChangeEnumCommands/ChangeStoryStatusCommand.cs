﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WIM.Commands.Abstracts;
using WIM.Models.Enums;
using WIM.Models.Units;

namespace WIM.Commands.CommandClasses
{
    public class ChangeStoryStatusCommand : Command
    {
        public ChangeStoryStatusCommand(IList<string> commandParameters) : base(commandParameters)
        {
        }

        public override string Execute()
        {


            StringBuilder storySb = new StringBuilder();

            try
            {

                bool isValidId = int.TryParse(CommandParameters[0], out int id);

                bool isStoryStatus = Enum.TryParse<StoryStatus>(CommandParameters[1], true, out StoryStatus storyStatus);

                var storyToChangeStatus = this.Database.Story.FirstOrDefault(x => x.Id == id);

                if (!isStoryStatus)
                { 

                    throw new ArgumentException($"{CommandParameters[1]} is not а valid story status!");

                }

                else if (!this.Database.Story.Any(x => x.Id == id))

                {
                    return storySb.AppendLine($"Story with ID {CommandParameters[0]} doesnt exsist!").ToString();
                }


                else if (storyToChangeStatus.StoryStatus.Equals(storyStatus))

                {
                    return storySb.AppendLine($"Story with Id: {id} was not changed , it already has status: {storyStatus}").ToString();
                }


                storySb.AppendLine($"Bug with Id:{id} status was changed from {storyToChangeStatus.StoryStatus} to {storyStatus} status!");

                storyToChangeStatus.StoryStatus = storyStatus;

                storyToChangeStatus.History.Add(new ActivityHistory(storySb.ToString()));

                return storySb.ToString();
            }


            catch
            {
                throw new ArgumentException("Failed to parse \"Change Bug Severity\" command parameters.");
            }
        }
    }
}
