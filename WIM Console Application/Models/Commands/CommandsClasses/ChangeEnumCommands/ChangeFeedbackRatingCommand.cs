﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WIM.Commands.Abstracts;
using WIM.Models.Units;

namespace WIM.Commands.CommandClasses
{
    public class ChangeFeedbackRatingCommand : Command
    {
        public ChangeFeedbackRatingCommand(IList<string> commandParameters) : base(commandParameters)
        {
        }

        public override string Execute()
        {


            StringBuilder feedbackSb = new StringBuilder();

            try
            {

                bool isValidId = int.TryParse(CommandParameters[0], out int id);

                bool isvalidRating = int.TryParse(CommandParameters[1], out int rating);

                var feedbackToChangeRating = this.Database.Feedback.FirstOrDefault(x => x.Id == id);

                if (!isValidId || id < 0 || id > 10)

                {

                    feedbackSb.AppendLine($"{CommandParameters[1]} is not a valid feedback rating");

                    return feedbackSb.ToString();

                }

                else if (feedbackToChangeRating.Rating.Equals(rating))

                {
                    feedbackSb.AppendLine($"Feedback with Id: {id} already has the same rating: {rating}");

                    return feedbackSb.ToString();
                }

                else

                {

                    feedbackToChangeRating.Rating = rating;

                    feedbackSb.AppendLine($"Feedback with Id:{id} rating was updated to rating{rating}");

                    feedbackToChangeRating.History.Add(new ActivityHistory(feedbackSb.ToString()));

                    return feedbackSb.ToString();
                }
            }


            catch
            {
                throw new ArgumentException("Failed to parse \"Change Bug Severity\" command parameters.");
            }
        }
    }
}
