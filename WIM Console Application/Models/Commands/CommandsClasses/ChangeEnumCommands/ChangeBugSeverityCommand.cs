﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WIM.Commands.Abstracts;
using WIM.Models.Enums;
using WIM.Models.Units;

namespace WIM.Commands.CommandClasses
{
    public class ChangeBugSeverityCommand : Command
    {
        public ChangeBugSeverityCommand(IList<string> commandParameters) : base(commandParameters)
        {
        }

        public override string Execute()
        {


            StringBuilder bugSb = new StringBuilder();

            try
            {

                bool isValidId = int.TryParse(CommandParameters[0], out int result);

                bool isBugSeverity = Enum.TryParse<Severity>(CommandParameters[1], true, out Severity bugSeverity);

                var bugToChangeSeverity = this.Database.Bug.FirstOrDefault(x => x.Id == result);

                if (!isBugSeverity)

                {

                    throw new ArgumentException($"{CommandParameters[1]} is not а valid bug severity!");

                }
                else if (!isValidId)
                {

                    bugSb.AppendLine($"{CommandParameters[0]} is not a valid bug Id");

                    return bugSb.ToString();

                }

                else if (!this.Database.Bug.Any(x => x.Id == result))

                {

                    return bugSb.AppendLine($"Feedback with ID {CommandParameters[0]} doesnt exsist!").ToString();

                }


                else if (bugToChangeSeverity.Severity.Equals(bugSeverity))

                {

                    return bugSb.AppendLine($"Bug with Id: {result} was not changed , it already has severity: {bugSeverity}").ToString();

                }


                bugSb.AppendLine($"Bug with Id:{result} status was changed from {bugToChangeSeverity.Severity} to {bugSeverity} status!");

                bugToChangeSeverity.Severity = bugSeverity;

                bugToChangeSeverity.History.Add(new ActivityHistory(bugSb.ToString()));

                return bugSb.ToString();
            }


            catch
            {

                throw new ArgumentException("Failed to parse \"Change Bug Severity\" command parameters.");

            }
        }
    }
}

