﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WIM.Commands.Abstracts;
using WIM.Models.Enums;
using WIM.Models.Units;

namespace WIM.Commands.CommandClasses
{
    public class ChangeStoryPriorityCommand : Command
    {
        public ChangeStoryPriorityCommand(IList<string> commandParameters) : base(commandParameters)
        {
        }

        public override string Execute()
        {


            StringBuilder storySb = new StringBuilder();

            try
            {

                bool isValidId = int.TryParse(CommandParameters[0], out int result);

                bool isStoryPriority = Enum.TryParse<Priority>(CommandParameters[1], true, out Priority storyPriority);

                var storyToChangePriority = this.Database.Story.FirstOrDefault(x => x.Id == result);

                if (!isStoryPriority)

                { 

                    throw new ArgumentException($"{CommandParameters[1]} is not а valid story priority!");

                }

                else if (!isValidId)
                {
                    storySb.AppendLine($"{CommandParameters[0]} is not a valid story Id");

                    return storySb.ToString();
                }

                else if (!this.Database.Story.Any(x => x.Id == result))

                {
                    return storySb.AppendLine($"Story with ID {CommandParameters[0]} doesnt exsist!").ToString();
                }


                else if (storyToChangePriority.Priority.Equals(storyPriority))

                {
                    return storySb.AppendLine($"Story with Id: {result} was not changed , it alreade has priority: {storyPriority}").ToString();
                }


                storySb.AppendLine($"Bug with Id:{result} status was changed from {storyToChangePriority.Priority} to {result} status!");

                storyToChangePriority.Priority = storyPriority;

                storyToChangePriority.History.Add(new ActivityHistory(storySb.ToString()));

                return storySb.ToString();
            }


            catch
            {
                throw new ArgumentException("Failed to parse \"Change Bug Priority\" command parameters.");
            }
        }
    }
}
