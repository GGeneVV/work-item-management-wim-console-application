﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WIM.Commands.Abstracts;
using WIM.Models.Enums;
using WIM.Models.Units;

namespace WIM.Commands.CommandClasses
{
    public class ChangeFeedbackStatusCommand : Command
    {
        public ChangeFeedbackStatusCommand(IList<string> commandParameters) : base(commandParameters)
        {
        }

        public override string Execute()
        {
            StringBuilder sb = new StringBuilder();

            var isNumeric = int.TryParse(CommandParameters[0], out int feedbackId);

            var feedbackToChangeStatus = this.Database.Feedback.FirstOrDefault(x => x.Id == feedbackId);

            if (this.Database.Feedback.Count == 0)
            {
                sb.AppendLine("No feedback in database!");
            }

            else if (!Enum.TryParse<FeedbackStatus>(CommandParameters[1], true, out FeedbackStatus status))
            {
                throw new ArgumentException($"{CommandParameters[1]} is not valid feedback status!");
            }

            else if ((!this.Database.Feedback.Any(n => n.Id == feedbackId)) || !isNumeric )
            {
                sb.AppendLine($"Feedback with ID {CommandParameters[0]} doesnt exsist!");
            }

            else
            {
                feedbackToChangeStatus.FeedbackStatus = status;
                sb.AppendLine($"Status on feedback with ID {feedbackToChangeStatus.Id} was changed to {CommandParameters[1]}");

                feedbackToChangeStatus.History.Add(new ActivityHistory(sb.ToString()));
            }


            return sb.ToString();
        }
    }
}
