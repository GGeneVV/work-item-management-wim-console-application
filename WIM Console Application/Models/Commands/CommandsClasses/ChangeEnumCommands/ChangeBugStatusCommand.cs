﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WIM.Commands.Abstracts;
using WIM.Models.Enums;
using WIM.Models.Units;

namespace WIM.Commands.CommandClasses
{
    public class ChangeBugStatusCommand : Command
    {
        public ChangeBugStatusCommand(IList<string> commandParameters) : base(commandParameters)
        {
        }

        public override string Execute()
        {


            StringBuilder bugSb = new StringBuilder();

            try
            {

                bool isValidId = int.TryParse(CommandParameters[0], out int result);

                bool isBugStatus = Enum.TryParse<BugStatus>(CommandParameters[1], true, out BugStatus bugStatus);

                var bugToChangeStatus = this.Database.Bug.FirstOrDefault(x => x.Id == result);

                if (!isBugStatus)

                {

                    bugSb.AppendLine($"ArgumentException was thrown:{CommandParameters[1]} is not a vali bug status");


                    throw new ArgumentException($"{CommandParameters[1]} is not а valid bug status!");
                }

                else if (!this.Database.Bug.Any(x => x.Id == result))

                {
                    return bugSb.AppendLine($"Feedback with ID {CommandParameters[0]} doesnt exsist!").ToString();
                }


                else if (bugToChangeStatus.BugStatus.Equals(bugStatus))

                {
                    return bugSb.AppendLine($"Bug with Id: {result} was not changed , it alreade has status: {bugStatus}").ToString();
                }


                bugSb.AppendLine($"Bug with Id:{result} status was changed from {bugToChangeStatus.BugStatus} to {bugStatus} status!");

                bugToChangeStatus.BugStatus = bugStatus;

                bugToChangeStatus.History.Add(new ActivityHistory(bugSb.ToString()));

                return bugSb.ToString();
            }


            catch
            {
                throw new ArgumentException("Failed to parse \"Change Bug Severity\" command parameters.");
            }
        }
    }
}
