﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WIM.Commands.Abstracts;
using WIM.Models.Enums;
using WIM.Models.Units;

namespace WIM.Commands.CommandClasses
{

    public class ChangeBugPriorityCommand : Command
    {
        public ChangeBugPriorityCommand(IList<string> commandParameters) : base(commandParameters)
        {
        }

        public override string Execute()
        {


            StringBuilder bugSb = new StringBuilder();

            try
            {

                bool isValidId = int.TryParse(CommandParameters[0], out int result);

                bool isBugPriority = Enum.TryParse<Priority>(CommandParameters[1], true, out Priority bugPriority);

                var bugToChangePriority = this.Database.Bug.FirstOrDefault(x => x.Id == result);

                

                if (!isBugPriority)

                {

                    throw new ArgumentException($"{CommandParameters[1]} is not а valid bug priority!");

                }

                else if (!isValidId)
                {

                    bugSb.AppendLine($"{CommandParameters[0]} is not a valid bug Id");

                    return bugSb.ToString();

                }

                else if (!this.Database.Bug.Any(x => x.Id == result))

                {

                    return bugSb.AppendLine($"Feedback with ID {CommandParameters[0]} doesnt exsist!").ToString();

                }


                else if (bugToChangePriority.BugStatus.Equals(bugPriority))

                {

                    return bugSb.AppendLine($"Bug with Id: {result} was not changed , it already has priority: {bugPriority}").ToString();

                }


                bugSb.AppendLine($"Bug with Id:{result} status was changed from {bugToChangePriority.BugStatus} to {bugPriority} status!");

                bugToChangePriority.Priority = bugPriority;

                bugToChangePriority.History.Add(new ActivityHistory(bugSb.ToString()));

                return bugSb.ToString();
            }


            catch
            {

                throw new ArgumentException("Failed to parse \"Change Bug Priority\" command parameters.");

            }

        }
    }
}


