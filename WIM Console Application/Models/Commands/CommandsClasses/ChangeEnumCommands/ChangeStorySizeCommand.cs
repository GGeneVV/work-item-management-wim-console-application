﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WIM.Commands.Abstracts;
using WIM.Models.Enums;
using WIM.Models.Units;

namespace WIM.Commands.CommandClasses
{
    public class ChangeStorySizeCommand : Command
    {
        public ChangeStorySizeCommand(IList<string> commandParameters) : base(commandParameters)
        {
        }

        public override string Execute()
        {


            StringBuilder storySb = new StringBuilder();

            try
            {

                bool isValidId = int.TryParse(CommandParameters[0], out int id);

                bool isStorySize = Enum.TryParse<StorySize>(CommandParameters[1], true, out StorySize storySize);

                var storyToChangeSize = this.Database.Story.FirstOrDefault(x => x.Id == id);

                if (!isStorySize)

                {

                    throw new ArgumentException($"{CommandParameters[1]} is not а valid story size!");

                }

                else if (!isValidId)

                {

                    storySb.AppendLine($"{CommandParameters[0]} is not a valid story Id");

                    return storySb.ToString();

                }

                else if (!this.Database.Story.Any(x => x.Id == id))

                {

                    return storySb.AppendLine($"Story with ID {CommandParameters[0]} doesnt exsist!").ToString();

                }


                else if (storyToChangeSize.Size.Equals(storySize))

                {
                    return storySb.AppendLine($"Story with Id: {id} was not changed , it already has size: {storySize}").ToString();
                }


                storySb.AppendLine($"Story with Id:{id} size was changed from {storyToChangeSize.Size} to {id} status!");

                storyToChangeSize.Size = storySize;

                storyToChangeSize.History.Add(new ActivityHistory(storySb.ToString()));

                return storySb.ToString();
            }


            catch
            {
                throw new ArgumentException("Failed to parse \"Change Story Size\" command parameters.");
            }
        }
    }
}
