﻿using System;
using System.Collections.Generic;
using System.Linq;
using WIM.Commands.Abstracts;
using WIM.Models.Enums;
using WIM.Models.Units;

namespace WIM.Commands.Contracts
{
    public class CreateBugCommand : Command
    {
        public CreateBugCommand(IList<string> commandParameters)
            : base(commandParameters)
        {
        }

        public override string Execute()
        {
            try
            {
                string title = CommandParameters[0];

                string description = CommandParameters[1];

                Enum.TryParse<BugStatus>(CommandParameters[2], true, out BugStatus status);

                Enum.TryParse<Severity>(CommandParameters[3], true, out Severity severity);

                Enum.TryParse<Priority>(CommandParameters[4], true, out Priority priority);

                string stepsToReproduce = CommandParameters[5].ToString();

                if (this.Database.Bug.Any(x => x.Title == title))
                {
                    throw new Exception($"Bug with name {title} already exist");
                }
                else
                {

                    var bug = this.Factory.CreateBug(title, description, status, severity, priority, stepsToReproduce);
                    this.Database.Bug.Add(bug);

                    bug.History.Add(new ActivityHistory($"Bug with title {title} and {description} " +
                        $"{status}, {severity}, {priority} has been created. Please follow the steps to reproduce" +
                        $"{stepsToReproduce}"));


                    return $"Bug with ID {bug.Id} {title} and description: {description} was created.";
                }
            }

            catch
            {
                throw new ArgumentException("Failed to parse Create Bug command parameters.");
            }


        }

    }
}
