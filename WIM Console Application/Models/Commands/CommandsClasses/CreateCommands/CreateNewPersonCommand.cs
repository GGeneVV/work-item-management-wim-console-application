﻿using System;
using System.Collections.Generic;
using WIM.Commands.Abstracts;
using System.Linq;
using WIM.Models.Units;

namespace WIM.Commands.CommandClasses
{
    public class CreateNewPersonCommand :Command
    {
        public CreateNewPersonCommand(IList<string> commandParameters)
            : base(commandParameters)
        {
        }
        public override string Execute()
        {
            try
            {
                var personName = CommandParameters[0];

                if (this.Database.Members.Any(x => x.Name == personName))
                {
                    throw new Exception("Member names must be unique");
                }

                var member = this.Factory.CreateMember(personName);

                this.Database.Members.Add(member);

                member.AddActivityHistory(new ActivityHistory($"Member named {member.Name} was created."));

                return $"Member named {member.Name} was created.";


            }

            catch (Exception)
            {

                throw new Exception("Invalid member name!");

            }

        }
    }
}
