﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using WIM.Commands.Abstracts;
using WIM.Models.Enums;

using WIM.Models.Units;


namespace WIM.Commands.CommandClasses
{

    public class CreateNewBoardInTeamCommand : Command
    {

        public CreateNewBoardInTeamCommand(IList<string> commandParameters)
            : base(commandParameters)
        {
        }
        public override string Execute()
        {
            try
            {
                var boardName = CommandParameters[0];

                var teamName = CommandParameters[1];

                var boardToAdd = this.Factory.CreateBoard(boardName);

                var teamToAdTo = this.Database.Teams.FirstOrDefault(x => x.Name == teamName);

                if (teamToAdTo == null)
                {
                    throw new ArgumentNullException("Team does not exist in the Database");
                }

                teamToAdTo.AddBoard(boardToAdd);

                teamToAdTo.AddActivityHistory(new ActivityHistory($"Board {boardName} was added to team {teamName} "));

                boardToAdd.AddActivityHistory(new ActivityHistory($"Board {boardName} was created"));

                return $"Board with named: {boardName} was created for team {teamName}.";


            }

            catch (Exception)
            {

                throw new Exception("Invalid member name!");

            }
            
        }
    }

}
