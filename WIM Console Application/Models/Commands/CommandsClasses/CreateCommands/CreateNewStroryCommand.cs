﻿using System;
using System.Collections.Generic;
using WIM.Commands.Abstracts;
using WIM.Models.Enums;


namespace WIM.Commands.CommandClasses
{

    public class CreateNewStoryCommand : Command
    {
        public CreateNewStoryCommand(IList<string> commandParameters)
           : base(commandParameters)
        {
        }
        public override string Execute()
        {

            try
            {
                string title = CommandParameters[0];

                string description = CommandParameters[1];


                Enum.TryParse<StorySize>(CommandParameters[3], true, out StorySize size);

                Enum.TryParse<Priority>(CommandParameters[2], true, out Priority priority);

                Enum.TryParse<StoryStatus>(CommandParameters[4], true, out StoryStatus status);

                var story = this.Factory.CreateStory(title, description, size, priority, status);

                this.Database.Story.Add(story);

                return $"Story with ID {story.Id} was created.";
            }

            catch
            {
                throw new ArgumentException("Failed to parse CreateStory command parameters.");
            }

        }
    }

}
