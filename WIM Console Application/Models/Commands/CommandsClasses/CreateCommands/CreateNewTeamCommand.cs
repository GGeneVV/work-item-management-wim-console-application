﻿using System;
using System.Collections.Generic;
using System.Linq;
using WIM.Commands.Abstracts;
using WIM.Models.Units;

namespace WIM.Commands.CommandClasses
{
    public class CreateNewTeamCommand : Command
    {
        public CreateNewTeamCommand(IList<string> commandParameters)
            : base(commandParameters)
        {
        }

        public override string Execute()
        {
            try
            {
                var teamName = CommandParameters[0];

                if (this.Database.Teams.Any(x => x.Name == teamName))
                {
                    throw new Exception("Team names must be unique");
                }
                var team = this.Factory.CreateTeam(teamName);

                this.Database.Teams.Add(team);

                team.AddActivityHistory(new ActivityHistory($"Team with named {teamName} was created."));

                return $"Team with named {teamName} was created.";

            }
            catch (Exception)
            {

                throw new Exception("Invalid team name!");
            }
          
        }
    }
}
