﻿using System;
using System.Collections.Generic;
using WIM.Commands.Abstracts;
using WIM.Models.Enums;
using WIM.Models.Units;

namespace WIM.Commands.CommandsClasses.CreateCommands
{
    public class CreateFeedbackCommand:Command
    {
        public CreateFeedbackCommand(IList<string> commandParameters)
           : base(commandParameters)
        {
        }

        public override string Execute()
        {
            try
            {
                string title = CommandParameters[0];

                string description = CommandParameters[1];

                int.TryParse(CommandParameters[2], out int rating);

                Enum.TryParse<FeedbackStatus>(CommandParameters[3], true, out FeedbackStatus status);

                var feedback = this.Factory.CreateFeedback(title, description,rating, status);

                this.Database.Feedback.Add(feedback);
                
                feedback.History.Add(new ActivityHistory($"Feedback with title: {title},description: {description}," +
                    $"rating: {rating} and status: {status} was created."));


                return $"Feedback with title: {title},description: {description}," +
                    $"rating: {rating} and status: {status} was created.";
            }

            catch
            {
                throw new ArgumentException("Failed to parse Create Feedback command parameters.");
            }

        }
    }
}
