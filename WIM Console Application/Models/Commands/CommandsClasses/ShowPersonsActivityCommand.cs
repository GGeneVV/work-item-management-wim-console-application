﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WIM.Commands.Abstracts;

namespace WIM.Commands.CommandClasses
{
    public class ShowPersonsActivityCommand : Command
    {
        public ShowPersonsActivityCommand(IList<string> commandParameters) : base(commandParameters)
        {
        }
        public override string Execute()
        {
            StringBuilder sb = new StringBuilder();
            var personToShowActivity = this.Database.Members.Where(x => x.Name == CommandParameters[0]);

            if (personToShowActivity==null)
            {
                throw new Exception("No person in the list");
            }
            personToShowActivity.ToString();

            

           foreach (var item in personToShowActivity)
            {
               sb.AppendLine(item.ToString());
           }
            return sb.ToString();
        }
    }
}
