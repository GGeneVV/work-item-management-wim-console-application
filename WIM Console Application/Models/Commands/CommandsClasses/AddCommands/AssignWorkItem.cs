﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Security.Cryptography.X509Certificates;
using System.Xml.Schema;
using WIM.Commands.Abstracts;
using WIM.Models.Units;

namespace WIM.Commands.CommandClasses
{

    public class AssignWorkItem : Command
    {


        public AssignWorkItem(IList<string> commandParameters)
           : base(commandParameters)
        {
        }

        public override string Execute()
        {

            bool workItemIdCheck;

            string personName;

            string itemType;

            try
            {

                workItemIdCheck = int.TryParse(CommandParameters[0], out int result);

                personName = CommandParameters[1];

                itemType = CommandParameters[2];

                if (!workItemIdCheck)
                {
                    throw new Exception("Not a valid Id");
                }



                if (itemType == "bug")
                {


                    var bugToAssign = this.Database.Bug.FirstOrDefault(x => x.Id == result);

                    var member = this.Database.GetMember(personName);

                    if (bugToAssign != null && member != null)

                    {
                        bugToAssign.Assignee = member;

                        bugToAssign.History.Add(new ActivityHistory($"Bug :{bugToAssign.Title} was successfully assigned to {member.Name}"));

                        return $"{bugToAssign.Title} was  successfully assigned to:{member.Name}.";
                    }

                    bugToAssign.History.Add(new ActivityHistory($"Bug :{bugToAssign.Title} could not be assigned to {member.Name}"));
                   
                    return "Bug was not assigned successfully , please try again";
                }
                else
                {
                    var storyToAssign = this.Database.Story.FirstOrDefault(x => x.Id == result);

                    var member = this.Database.GetMember(personName);

                    if (storyToAssign != null && member != null)
                    {
                        storyToAssign.Assignee = member;

                        storyToAssign.History.Add(new ActivityHistory($"Bug :{storyToAssign.Title} could not be assigned to {member.Name}"));
                       
                        return $"{member.Name} was successfully assigned to: {storyToAssign.Title} ";
                    }

                    storyToAssign.History.Add(new ActivityHistory($"Bug :{storyToAssign.Title} could not be assigned to {member.Name}"));

                    return "Story was not assigned successfully , please try again";
                }
            }

            catch
            {
                throw new ArgumentException("Failed to parse \"Add Comment to Work Item\" command parameters.");
            }

        }
    }
}

