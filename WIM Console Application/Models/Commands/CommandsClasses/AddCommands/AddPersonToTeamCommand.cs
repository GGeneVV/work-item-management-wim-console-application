﻿using System;
using System.Collections.Generic;
using System.Linq;
using WIM.Commands.Abstracts;
using WIM.Models.Units;


namespace WIM.Commands.CommandClasses
{
    public class AddPersonToTeamCommand : Command
    {


        public AddPersonToTeamCommand(IList<string> commandParameters)
           : base(commandParameters)
        {
        }



        public override string Execute()
        {

            string nameOfPerson;
            string nameOfTeam;

            try
            {

                nameOfPerson = CommandParameters[0];

                nameOfTeam = CommandParameters[1];

                var teamToAddTo = this.Database.Teams.FirstOrDefault(x => x.Name == nameOfTeam);

                var memberToAdd = this.Database.Members.FirstOrDefault(x => x.Name == nameOfPerson);


                teamToAddTo.AddMember(memberToAdd);

                teamToAddTo.AddActivityHistory(new ActivityHistory($"{nameOfPerson} was added to Team: {nameOfTeam}"));

                memberToAdd.AddActivityHistory(new ActivityHistory($"{nameOfPerson} joined Team: {nameOfTeam}"));



                return $"New member:{nameOfPerson} was  successfully added to Team:{nameOfTeam}.";
            }

            catch
            {
                throw new ArgumentException("Failed to parse \"Add Comment to Work Item\" command parameters.");
            }

        }
    }
}

