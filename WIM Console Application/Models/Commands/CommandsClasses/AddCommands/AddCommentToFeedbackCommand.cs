﻿using System;
using System.Collections.Generic;
using System.Linq;
using WIM.Commands.Abstracts;
using WIM.Models.Units;

namespace WIM.Commands.CommandClasses
{
    public class AddCommentToFeedbackCommand : Command
    {
        public AddCommentToFeedbackCommand(IList<string> commandParameters)
          : base(commandParameters)
        {
        }



        public override string Execute()
        {
            int id;
            string author;
            string commentText;
            try
            {
                id = int.Parse(CommandParameters[0]);

                author = CommandParameters[1];

                commentText = CommandParameters[2];

                var feedback = this.Database.Feedback.FirstOrDefault(x => x.Id == id);

                feedback.Comments.Add(new Comment(author, commentText));

                feedback.History.Add(new ActivityHistory($"{author} added the following " +
                    $"comment to bug: {commentText}"));

                return $"New comment was added to feedback with Id:{feedback.Id} by Author:{author}" +
                       $"Comment: {commentText}.";
            }

            catch
            {
                throw new ArgumentException("Failed to parse \"Add Comment to Feedback\" command parameters.");
            }

        }
    }
}
