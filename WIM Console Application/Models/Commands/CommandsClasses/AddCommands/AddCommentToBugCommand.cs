﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using WIM.Commands.Abstracts;
using WIM.Models.Abstract;
using WIM.Models.Units;

namespace WIM.Commands.CommandClasses
{
    public class AddCommentToBugCommand : Command
    {

        public AddCommentToBugCommand(IList<string> commandParameters)
           : base(commandParameters)
        {
        }



        public override string Execute()
        {
            int id;
            string author;
            string commentText;
            try
            {
                id = int.Parse(CommandParameters[0]);
                
                if (string.IsNullOrEmpty(CommandParameters[1]) || string.IsNullOrEmpty(CommandParameters[2]))
                {
                    throw new ArgumentException();
                }
                author =  CommandParameters[1];


                commentText = CommandParameters[2];

                var bug = this.Database.Bug.FirstOrDefault(x => x.Id == id);

                bug.Comments.Add(new Comment(author, commentText));

                bug.History.Add(new ActivityHistory($"{author} added the following " +
                    $"comment to bug: {commentText}"));

            return $"{author} added the following comment to bug: {commentText}";

            }

            catch
            {
                throw new ArgumentException("Failed to parse \"Add Comment to Work Item\" command parameters.");
            }


        }
    }
}
