﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Security.Cryptography.X509Certificates;
using System.Xml.Schema;
using WIM.Commands.Abstracts;
using WIM.Models.Units;

namespace WIM.Commands.CommandsClasses.AddCommands
{
    public class UnassignWorkItem : Command
    {


        public UnassignWorkItem(IList<string> commandParameters)
           : base(commandParameters)
        {
        }



        public override string Execute()
        {

            bool workItemIdCheck;

            string personName;

            string itemType;

            try
            {

                workItemIdCheck = int.TryParse(CommandParameters[0], out int result);

                personName = CommandParameters[1];

                itemType = CommandParameters[2];

                if (!workItemIdCheck)
                {
                    throw new Exception("Not a valid Id");
                }



                if (itemType == "bug")
                {


                    var bugToAssign = this.Database.Bug.FirstOrDefault(x => x.Id == result);

                    var member = this.Database.GetMember(personName);


                    if (bugToAssign.Assignee==member)
                    {
                        bugToAssign.Assignee=null;

                        bugToAssign.History.Add(new ActivityHistory($"Bug :{bugToAssign.Title} was successfully unassigned to {member.Name}"));

                        return $"Bug with name {bugToAssign.Title} was removed from member {member.Name} ";
                    }

                    return $"No such member assigned to {member.Name}";
 
                }


                else if(itemType == "story")

                {
                    var storyToAssign = this.Database.Story.FirstOrDefault(x => x.Id == result);

                    var member = this.Database.GetMember(personName);

                    if (storyToAssign.Assignee==member)
                    {
                        storyToAssign.Assignee = null;

                        storyToAssign.History.Add(new ActivityHistory($"Bug :{storyToAssign.Title} was successfully assigned to {member.Name}"));

                        return $"{member.Name} was successfully unassigned from story: {storyToAssign.Title} ";
                    }

                    return "Story was not assigned successfully , please try again";
                }
                else
                {
                    return "No such item";
                }



            }

            catch
            {
                throw new ArgumentException("Failed to find assignee.");
            }

        }
    }
}


