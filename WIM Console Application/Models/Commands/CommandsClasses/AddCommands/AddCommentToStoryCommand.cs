﻿using System;
using System.Collections.Generic;
using System.Linq;
using WIM.Commands.Abstracts;
using WIM.Models.Units;

namespace WIM.Commands.CommandClasses
{

    public class AddCommentToStoryCommand : Command
    {
        public AddCommentToStoryCommand(IList<string> commandParameters)
          : base(commandParameters)
        {
        }



        public override string Execute()
        {
            int id;
            string author;
            string commentText;

            try
            {
                id = int.Parse(CommandParameters[0]);

                author = CommandParameters[1];

                commentText = CommandParameters[2];

                var story = this.Database.Story.FirstOrDefault(x => x.Id == id);

                story.Comments.Add(new Comment(author, commentText));

                return $"New comment was added to story with Id:{story.Id} by Author:{author}" +
                       $"Comment: {commentText}.";
            }

            catch
            {
                throw new ArgumentException("Failed to parse \"Add Comment to Story\" command parameters.");
            }


        }
    }
}
