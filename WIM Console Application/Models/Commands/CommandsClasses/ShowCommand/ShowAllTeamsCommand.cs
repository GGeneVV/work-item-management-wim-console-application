﻿using System;
using System.Collections.Generic;
using System.Text;
using WIM.Commands.Abstracts;

namespace WIM.Commands.CommandClasses
{
    public class ShowAllTeamsCommand :Command
    {
        public ShowAllTeamsCommand(IList<string> commandParameters) : base(commandParameters)
        {
        }
        public override string Execute()
        {
            StringBuilder sb = new StringBuilder();
            int counter = 1;
            sb.AppendLine("All teams ");
            foreach (var item in this.Database.Teams)
            {
                sb.AppendLine($"{counter}. {item.Name.ToString()}");
                counter++;
            }
            return sb.ToString();
        }
    }
}
