﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WIM.Commands.Abstracts;

namespace WIM.Commands.CommandClasses
{
    public class ShowAllPeopleCommand : Command
    {
        public ShowAllPeopleCommand(IList<string> commandParameters) : base(commandParameters)
        {
        }

        public override string Execute()
        {
            StringBuilder sb = new StringBuilder();

            return this.Database.Members.Count > 0 ?
                string.Join(Environment.NewLine, this.Database.Members.ToList())
                : "No members to show!";
        }
    }
}
