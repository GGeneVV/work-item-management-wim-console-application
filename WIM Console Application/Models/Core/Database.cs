﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using WIM.Core.Contracts;
using WIM.Models.Contracts;
using WIM.Models.Units;

namespace WIM.Core
{
    public class Database : IDatabase
    {
        private static IDatabase instance = null;
        public static IDatabase Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new Database();
                }
                return instance;
            }
        }           
        private  List<IBug> bugs = new List<IBug>();
        private readonly  List<IStory> stories = new List<IStory>();
        private readonly  List<IFeedback> feedbacks = new List<IFeedback>();
        private readonly  List<IMember> members = new List<IMember>();
        private readonly List<IBoard> boards = new List<IBoard>();
        private readonly  List<ITeam> teams = new List<ITeam>();

        public List<IBug> Bug => this.bugs; 
        public List<IStory> Story => this.stories;
        public List<IFeedback> Feedback => this.feedbacks;
        public List <IMember> Members => this.members;
        public List<ITeam> Teams => this.teams;
        public List<IBoard> Boards => this.boards;

        public IBug GetBugId(int id)
        {            
            var itemId = Bug.FirstOrDefault(x => x.Id == id);
            return itemId;
        }

        public IMember GetMember(string memberName)
        {
            var item = Members.FirstOrDefault(member => member.Name == memberName);
            return item;
        }

        public void AddStory(IStory story)
        {           
             this.Story.Add(story);            
        }
        public void AddBug(IBug bug)
        {           
            this.bugs.Add(bug);           
        }
        public void AddFeedback(IFeedback feedback)
        {
            this.feedbacks.Add(feedback);
        }


    }
}
















