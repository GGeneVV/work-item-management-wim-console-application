using System;
using System.Collections.Generic;
using System.Linq;
using WIM.Commands.CommandClasses;
using WIM.Commands.CommandsClasses.AddCommands;
using WIM.Commands.CommandsClasses.CreateCommands;
using WIM.Commands.Contracts;
using WIM.Core.Contracts;

namespace WIM.Core
{
    public class CommandManager : ICommandManager
    {
        public ICommand ParseCommand(string commandLine)
        {
            var lineParameters = commandLine
                .Trim()
                .Split(" \\", StringSplitOptions.RemoveEmptyEntries);

            string commandName = lineParameters[0];
            List<string> commandParameters = lineParameters.Skip(1).ToList();

            return commandName switch
            {
                "createbug" => new CreateBugCommand(commandParameters),
                "createfeedback" => new CreateFeedbackCommand(commandParameters),
                "createstory" => new CreateNewStoryCommand(commandParameters),
                "createperson" => new CreateNewPersonCommand(commandParameters),
                "createboard" => new CreateNewBoardInTeamCommand(commandParameters),
                "createteam" => new CreateNewTeamCommand(commandParameters),
                "addcommenttobug" => new AddCommentToBugCommand(commandParameters),
                "addcommenttostory" => new AddCommentToStoryCommand(commandParameters),
                "addcommenttofeedback" => new AddCommentToFeedbackCommand(commandParameters),
                "addperson" => new AddPersonToTeamCommand(commandParameters),
                "changebugpriority" => new ChangeBugPriorityCommand(commandParameters),
                "changebugseverity" => new ChangeBugSeverityCommand(commandParameters),
                "changebugstatus" => new ChangeBugStatusCommand(commandParameters),
                "changestorypriority" => new ChangeStoryPriorityCommand(commandParameters),
                "changestorysize" => new ChangeStorySizeCommand(commandParameters),
                "changestorystatus" => new ChangeStoryStatusCommand(commandParameters),
                "changefeedbackrating" => new ChangeFeedbackRatingCommand(commandParameters),
                "changefeedbackstatus" => new ChangeFeedbackStatusCommand(commandParameters),
                "assignworkitem" => new AssignWorkItem(commandParameters),
                "unassignworkitem" => new UnassignWorkItem(commandParameters),
                "showallpeople" => new ShowAllPeopleCommand(commandParameters),
                "showpersonsactivity" => new ShowPersonsActivityCommand(commandParameters),
                "showallteams" => new ShowAllTeamsCommand(commandParameters),
                "showteamsactivity" => new ShowTeamsActivityCommand(commandParameters),
                "addpersontoteam" => new AddPersonToTeamCommand(commandParameters),
                "showallteammembers" => new ShowAllTeamMembersCommand(commandParameters),
                "showallteamboards" => new ShowAllTeamBoardsCommand(commandParameters),
                "showboardsactivity" => new ShowBoardsActivityCommand(commandParameters),
                "showboardactivity" => new ShowBoardsActivityCommand(commandParameters),
                "listallbugs" => new ListAllBugsCommand(commandParameters),
                "listallstories" => new ListAllStoriesCommand(commandParameters),
                "listallfeedbacks" => new ListAllFeedbacksCommand(commandParameters),
                "listsortedbystatus" => new ListByStatusCommand(commandParameters),
                "listbystatusandassignee" => new FilterByStatusAndAssigneeCommand(commandParameters),
                "listbyassignee" => new FilterByAssigneeCommand(commandParameters),
                "listsortedbytitle" => new ListByTitleCommand(commandParameters),
                "listsortedbypriority" => new ListByPriorityCommand(commandParameters),
                "listsortedbyseverity" => new ListBySeverityCommand(commandParameters),
                "listsortedbysize" => new ListBySizeCommand(commandParameters),
                "listsortedbyrating" => new ListByRatingCommand(commandParameters),
                "filterbybassignee" => new FilterByAssigneeCommand(commandParameters),

                _ => throw new InvalidOperationException("command does not exist")
            };
        }
    }
}
