﻿using System;
using System.Collections.Generic;
using System.Text;
using WIM.Core.Contracts;
using WIM.Models.Contracts;
using WIM.Models.Enums;
using WIM.Models.Units;

namespace WIM.Core
{
    public class Factory : IFactory
    {
        private static IFactory instance;
        public static IFactory Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new Factory();
                }

                return instance;
            }
        }

        public IBug CreateBug(string title, string description, 
            BugStatus bugStatus, Severity severity, Priority priority, string stepsToReproduce)
        {
            Bug bug = new Bug(title, description, bugStatus, severity, priority, stepsToReproduce);
            return bug;
        }

        public IStory CreateStory(string title, string description, 
                     StorySize size, Priority priority, StoryStatus status)
        {
            Story story = new Story(title, description, size, priority, status);
            return story;
        }
        

        public IFeedback CreateFeedback(string title, string description, int rating, FeedbackStatus status)
        {
            Feedback feedback = new Feedback(title, description, rating, status);
            return feedback;
        }
        public IMember CreateMember(string name)
        {
            Member member = new Member(name);
            return member;
        }

        public IBoard CreateBoard(string name)
        {
            Board board = new Board(name);
            return board;
        }

        public ITeam CreateTeam(string name)
        {
            Team team = new Team(name);
            return team;
        }
    }
}







