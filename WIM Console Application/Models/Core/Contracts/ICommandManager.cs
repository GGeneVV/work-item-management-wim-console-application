﻿using System;
using System.Collections.Generic;
using System.Text;
using WIM.Commands.Contracts;

namespace WIM.Core.Contracts
{
    public interface ICommandManager
    {
        ICommand ParseCommand(string commandLine);
    }
}
