﻿using System;
using System.Collections.Generic;
using System.Text;
using WIM.Models.Contracts;
using WIM.Models.Enums;
using WIM.Models.Units;

namespace WIM.Core.Contracts
{
    public interface IFactory
    {
        public IBug CreateBug(string title, string description, BugStatus bugStatus, Severity severity, Priority priority, string stepsToReproduce);

        public IStory CreateStory(string title, string description, StorySize size, Priority priority, StoryStatus status);
        public IFeedback CreateFeedback(string title, string description, int rating, FeedbackStatus status);

        public IMember CreateMember(string name);

        public IBoard CreateBoard(string name);

        public ITeam CreateTeam(string name);
    }
}
