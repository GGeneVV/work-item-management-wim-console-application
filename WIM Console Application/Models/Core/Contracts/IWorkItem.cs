﻿using System;
using System.Collections.Generic;
using System.Text;
using WIM.Models.Units;

namespace WIM.Core.Contracts
{

    public interface IWorkItem
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public List<Comment> Comments { get; set; }
        public List<ActivityHistory> History { get; set; }

        


    }
}
