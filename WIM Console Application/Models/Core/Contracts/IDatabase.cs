﻿using System;
using System.Collections.Generic;
using System.Text;
using WIM.Models;
using WIM.Models.Contracts;

namespace WIM.Core.Contracts
{
    public interface IDatabase
    {


        List<IBug> Bug { get; }
        List<IStory> Story { get; }
        List<IFeedback> Feedback { get; }
        List<IMember> Members { get; }
        List<ITeam> Teams { get; }
        List<IBoard> Boards { get; }

        IBug GetBugId(int id);

        IMember GetMember(string memberName);

        void AddBug(IBug bug);

        void AddFeedback(IFeedback feedback);

        void AddStory(IStory story);

        
    }
}
