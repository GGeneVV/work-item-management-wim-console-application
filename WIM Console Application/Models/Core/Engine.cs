﻿using System;
using System.Collections.Generic;
using System.Text;
using WIM.Commands.Contracts;
using WIM.Core.Contracts;

namespace WIM.Core
{
    public class Engine 
    {
        private static Engine instance;
        public static Engine Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new Engine();
                }

                return instance;
            }
        }

        private readonly ICommandManager commandManager;

        private Engine()
        {
            this.commandManager = new CommandManager();
        }

        public void Run()
        {
            while (true)
            {
                // Read -> Process -> Print -> Repeat
                string input = this.Read();
                if (input == "exit")
                    break;


                string result = this.Process(input);

                this.Print(result);
            }
        }

        private string Read()
        {
            Console.WriteLine("Please input command and required parameters on a single line, separated by backslash.");
            string input = Console.ReadLine();
            return input;
        }

        private string Process(string commandLine)
        {
            try
            {
                ICommand command = this.commandManager.ParseCommand(commandLine);
                string result = command.Execute();

                return result.Trim();
            }
            catch (Exception e)
            {
                while (e.InnerException != null)
                {
                    e = e.InnerException;
                }

                return $"ERROR: {e.Message}";
            }
        }

        private void Print(string commandResult)
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine(commandResult);
            sb.AppendLine("<<<<<<<<<<>>>>>>>>>>");
            Console.WriteLine(sb.ToString().Trim());
        }
    }

}
