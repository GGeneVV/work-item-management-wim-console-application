﻿using System.Collections.Generic;
using WIM.Core.Contracts;

namespace WIM.Models.Contracts
{
    public interface ITeamElement
    {     
        public IReadOnlyList<IWorkItem> WorkItems { get; }

        public IReadOnlyList<IActivityHistory> ActivityHistory { get; }

        
    }
}
