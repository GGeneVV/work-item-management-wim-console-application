﻿using WIM.Models.Enums;
using WIM.Core.Contracts;
namespace WIM.Models.Contracts
{
    public interface IBug : IWorkItem
    {
        public BugStatus BugStatus { get; set; }

        public Severity Severity { get; set; }

        public Priority Priority { get; set; }

        public IMember Assignee { get; set; }

        public string StepsToReproduce { get; set; }


    }
}
