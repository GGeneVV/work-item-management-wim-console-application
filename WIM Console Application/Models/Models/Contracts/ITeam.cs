﻿using System.Collections.Generic;


namespace WIM.Models.Contracts
{
    public interface ITeam
    {
        string Name { get; }
        public IReadOnlyList<IMember> Members { get; }
        public IReadOnlyList<IBoard> Boards { get; }

        void AddBoard(IBoard board);

        void AddMember(IMember member);

        IReadOnlyList<IActivityHistory> ActivityHistories { get; }

        void AddActivityHistory(IActivityHistory activityHistory);
       
    }
}
