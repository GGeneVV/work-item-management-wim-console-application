﻿using System;

namespace WIM.Models.Contracts
{
    public interface IComment
    {
        public string Author { get; }
        public string Description { get; }
        public DateTime Time { get; }
    }
}
