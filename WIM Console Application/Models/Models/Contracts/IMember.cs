﻿

 namespace WIM.Models.Contracts
{
    public interface IMember
    {
        public string Name { get; }               
        public string ToString();

        public void AddActivityHistory(IActivityHistory history);
    }
}
