﻿using WIM.Core.Contracts;
using WIM.Models.Enums;

namespace WIM.Models.Contracts
{
    public interface IFeedback : IWorkItem
    {        
        public int Rating { get; set; }
        //TODO: Marin: we need date time and print
       // public DateTime Time { get; } 
       // public string Print();

        public FeedbackStatus FeedbackStatus { get; set; }

        
    }
}
