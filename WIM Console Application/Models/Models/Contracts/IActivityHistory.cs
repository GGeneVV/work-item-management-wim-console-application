﻿using System;

namespace WIM.Models.Contracts
{
    
    public interface IActivityHistory
    {
        string Description { get; }
        
        DateTime Time { get; }
    }
}
