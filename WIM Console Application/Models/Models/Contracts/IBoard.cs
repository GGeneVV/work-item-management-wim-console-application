﻿using System;
using System.Collections.Generic;
using WIM.Core.Contracts;

namespace WIM.Models.Contracts
{
    
    public interface IBoard
    {
        public string Name { get; }
        public IReadOnlyList<IWorkItem> WorkItems { get; }
        public IReadOnlyList<IActivityHistory> ActivityHistory { get; }

        public void AddItemToBoard(IWorkItem item);

        public void AddActivityHistory(IActivityHistory history);
    }
}
