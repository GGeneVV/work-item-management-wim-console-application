﻿using WIM.Core.Contracts;
using WIM.Models.Enums;

namespace WIM.Models.Contracts
{
    public interface IStory : IWorkItem
    {
        public StorySize Size { get; set; }
        public StoryStatus StoryStatus { get; set; }
        public Priority Priority { get; set; }
        public IMember Assignee { get; set; }
    }
}
