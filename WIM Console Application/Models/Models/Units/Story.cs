﻿using System;
using System.Text;
using WIM.Models.Abstract;
using WIM.Models.Contracts;
using WIM.Models.Enums;


namespace WIM.Models.Units
{
    public class Story : WorkItem, IStory
    {
        private IMember assignee;
        public Story(string title,
                     string description,
                     StorySize size,
                     Priority priority,
                     StoryStatus status)
               : base(title, description)
        {
            this.Size = size;
            this.Priority = priority;
            this.StoryStatus = status;
        }

        public StorySize Size { get; set; }

        public Priority Priority { get; set; }

        public StoryStatus StoryStatus { get; set; }

        public IMember Assignee
        {
            get => this.assignee;
            set
            {
                if (value == null)
                {
                    throw new Exception("You must have an assignee");
                }
                this.assignee = value;
            }
        }

        public override string ToString()
        {
            var baseInformation = base.ToString();
            StringBuilder sb = new StringBuilder();

            sb.Append(base.ToString());
            sb.AppendLine($"Story size: {this.Size}");
            sb.AppendLine($"Story priority: {this.Priority}");
            sb.AppendLine($"Story status: {this.StoryStatus}");
                           
            if (this.Assignee == null)
            {
                sb.AppendLine("Not assigned to Team/member!");
            }
            else
            {
                sb.AppendLine($"Assigned to: {this.Assignee}");
            }
            sb.Append("Comments:");
            if (this.Comments.Count < 1)
            {
                sb.AppendLine(" There is no comments to show!");
            }
            else
            {
                sb.AppendLine(Environment.NewLine + new string('#', 20));
                foreach (var item in this.Comments)
                {
                    sb.AppendLine(item.ToString());
                }
                sb.AppendLine(new string('#', 20));
                sb.AppendLine("End of comments!");
                sb.AppendLine(new string('#', 20));

            }
            return sb.ToString().Trim();

            /*$"Author: {this.Assignee} >>>>>{Line}" +
            $"Added the following comment: {this.Description}{Line}" +
            $"At >>> {this.Comments}{Line}";*/
        }
    }
}
