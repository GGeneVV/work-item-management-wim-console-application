﻿using System;
using WIM.Models.Contracts;

namespace WIM.Models.Units
{
    public class Comment :IComment
    {
        public Comment(string author,string description)
        {
            this.Author = author;
            this.Description = description;
            this.Time = DateTime.Now;
        }

        public string Author { get; }

        public string Description { get; }

        public DateTime Time { get; }

        public  string CommentPrint()
        {
            string Line = Environment.NewLine;

            return $"Author: {this.Author} >>>>>{Line}"+
                $"Added the following comment: {this.Description}{Line}"+
                $"At >>> {this.Time}{Line}";
        }
    }
}
