﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WIM.Models.Contracts;


namespace WIM.Models.Units
{
    public class Team : ITeam
    {
        private string name;
        private readonly List<IMember> members;
        private readonly List<IBoard> boards;
        private readonly List<IActivityHistory> activityHistories;

        public Team(string name)
        {
            this.Name = name;
            this.members = new List<IMember>();
            this.boards = new List<IBoard>();
            this.activityHistories = new List<IActivityHistory>();
        }

        public string Name //need to be 5-10 symbols....
        {
            get => this.name;
            set
            {
                if (string.IsNullOrWhiteSpace(value))
                {
                    throw new ArgumentNullException($"Team name can not be null or empty.{Environment.NewLine}" +
                        $"Please provide an corect Team name!");
                }

                else if (value == null || value.Length < 5 || value.Length > 10)
                {
                    throw new ArgumentException($"The name must be between five and ten symbols.");
                }
                else
                {
                    this.name = value;
                }

            }
        }

        public IReadOnlyList<IMember> Members => this.members;
        public IReadOnlyList<IBoard> Boards => this.boards;
        public IReadOnlyList<IActivityHistory> ActivityHistories => this.activityHistories;


        public override string ToString()
        {
            StringBuilder teamSb = new StringBuilder();

            

            if (Members.Count != 0)
            {
                teamSb.AppendLine($"Consisting of the the following Members");

                foreach (var member in Members)
                {
                    teamSb.AppendLine($"{member}");
                }
            }

            return base.ToString() + teamSb.ToString();
        }

        public void AddBoard(IBoard board)
        {
            if (this.Boards.FirstOrDefault(x => x.Name == board.Name) != null)
            {
                throw new Exception("Team already contains board");

            }

            this.boards.Add(board);
        }

        public void AddMember(IMember member)
        {
            if (this.Members.FirstOrDefault(x => x.Name == member.Name) != null)
            {
                throw new Exception("Member with the same name already exist in Team");
            }

            this.members.Add(member);
        }
        public void AddActivityHistory(IActivityHistory activityHistory)
        {
            this.activityHistories.Add(activityHistory);
        }
    }
}
