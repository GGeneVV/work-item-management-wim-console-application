﻿using System;
using WIM.Models.Abstract;
using WIM.Models.Contracts;

namespace WIM.Models.Units
{
    public class Board : TeamElement, IBoard
    {
        private string name;
        public Board(string name)
        {
            this.Name = name;
        }
        public string Name
        {
            get => this.name;
            set
            {
                if (value == null || value.Length < 5 || value.Length > 10)
                {
                    throw new ArgumentException($"The name must be between five and ten symbols.");
                }

                else
                {
                    this.name = value;
                }
            }
        }

        public virtual string ToString()
        {
            return base.ToString()+ $"Board {this.Name} was created.";
        }
       
    }
}
