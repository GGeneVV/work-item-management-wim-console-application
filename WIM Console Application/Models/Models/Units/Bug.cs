﻿using System;
using System.Text;
using WIM.Core.Contracts;
using WIM.Models.Abstract;
using WIM.Models.Contracts;
using WIM.Models.Enums;

namespace WIM.Models.Units
{
    public class Bug : WorkItem, IBug

    {
        private IMember assignee;


        public Bug(string title,
            string description,
            BugStatus bugStatus,
            Severity severity,
            Priority priority,
            string stepsToReproduce)
            : base(title, description)
        {
            this.Priority = priority;
            this.Severity = severity;
            this.BugStatus = bugStatus;
            this.StepsToReproduce = stepsToReproduce;

        }

        public Priority Priority { get; set; }

        public Severity Severity { get; set; }

        public BugStatus BugStatus { get; set; }

        public IMember Assignee
        {
            get => this.assignee;
            set
            {
                if (value == null)
                {
                    throw new Exception("You must have an assignee");
                }
                assignee = value;

            }
        }

        public string StepsToReproduce
        {
            get;
            set;
        }



        public override string ToString()
        {
            StringBuilder bugSb = new StringBuilder();
            
            bugSb.AppendLine($"Bug with {this.BugStatus} status , {this.Priority} and {this.Severity}");

            if (this.Assignee != null)
            {
                bugSb.AppendLine($"Was assigned to {this.Assignee.Name}");
            }
            else
            {
                bugSb.AppendLine($"Not assigned to Team/member!");
            }

            bugSb.AppendLine("Please follow these steps to reporoduce the Bug:");

            bugSb.AppendLine($"{this.StepsToReproduce}");

            return base.ToString() + bugSb.ToString().Trim();

        }




    }
}
