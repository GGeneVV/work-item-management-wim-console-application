﻿using System;
using System.Collections.Generic;
using System.Text;
using WIM.Core.Contracts;
using WIM.Models.Abstract;
using WIM.Models.Contracts;
using WIM.Models.Enums;

namespace WIM.Models.Units
{
    public class Feedback : WorkItem, IFeedback
    {
        private int rating;
        
        public Feedback(string title, string description, int rating, FeedbackStatus status)
           : base(title, description)
        {
            this.Rating = rating;
            this.FeedbackStatus = status;            
        }
        
        public int Rating
        {
            get => this.rating;
            set
            {
                if (value < 0 || value>10)
                {
                    throw new ArgumentOutOfRangeException("Feedback rating must be within 0 and 10");
                }
                rating = value;
            }
        }           

        public DateTime Time { get; set; }
        public FeedbackStatus FeedbackStatus { get; set; }

       

        public override string ToString()
        {
            var baseInformation = base.ToString();

            StringBuilder feedSb = new StringBuilder();

            feedSb.AppendLine();
            feedSb.Append($"{Environment.NewLine}Feedback ");
            feedSb.AppendLine(baseInformation);
            feedSb.AppendLine($"Feedback rating: {this.Rating};");
            feedSb.Append($"Feedback status: {this.FeedbackStatus};");
            feedSb.Append("Comments:");
            if (this.Comments.Count < 1)
            {
                feedSb.AppendLine(" There is no comments to show!");
            }
            else
            {
                feedSb.AppendLine(Environment.NewLine + new string('#', 20));
                foreach (var item in this.Comments)
                {
                    feedSb.AppendLine(item.ToString());
                }
                feedSb.AppendLine(new string('#', 20));
                feedSb.AppendLine("End of comments!");
                feedSb.AppendLine(new string('#', 20));
            }
            return feedSb.ToString();

          /*  return baseInformation + $"{newLine}Rating: {this.rating}" +
                $"{newLine}Status: {this.Status}";*/
        }
    }
}
