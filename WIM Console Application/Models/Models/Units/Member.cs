﻿using System;
using System.Linq;
using System.Text;
using WIM.Models.Abstract;
using WIM.Models.Contracts;

namespace WIM.Models.Units
{
    public class Member : TeamElement, IMember
    {
        private string name;
        public Member(string name)
            : base()
        {
            this.Name = name;
        }

        public string Name
        {
            get => this.name;
            set
            {
                if (string.IsNullOrWhiteSpace(value) || value.Length < 5 || value.Length > 15)
                {
                    throw new ArgumentException($"The name must be between five and fifteen symbols.");
                }
                else
                {
                    this.name = value;
                }
            }
        }


        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();

            sb.AppendLine(this.Name);

            foreach (var item in this.WorkItems)
            {
            sb.AppendLine(item.ToString());
            }
            sb.AppendLine("Activity hitsory:");

            if (this.ActivityHistory.Count < 1)

            {

                sb.AppendLine("There is no history.");

            }

             else

             {

                 sb.AppendLine(string.Join(Environment.NewLine, ActivityHistory));

             }
            return sb.ToString();
            //return base.ToString()+ $"{this.name} has joined the team!";
        }
    }
}
