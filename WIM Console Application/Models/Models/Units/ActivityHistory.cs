﻿using System;
using System.Collections.Generic;
using System.Text;
using WIM.Models.Contracts;

namespace WIM.Models.Units
{
    public class ActivityHistory : IActivityHistory
    {
        private string description;

        public ActivityHistory(string description)
        {
            this.Description = description;
            this.Time = DateTime.Now;
        }
        public string Description
        {
            get => this.description;

            set
            {
                if (string.IsNullOrWhiteSpace(value))
                {
                    throw new ArgumentNullException("Must have description");
                }
                if (value.Length < 10 || value.Length > 500)
                {
                    throw new ArgumentOutOfRangeException("The activity history description must be between 10 and 500 symbols");
                }
                description = value;
            }
        }

        public DateTime Time { get; set; }

        public string PrintActivityHistory()
        {
            string Line = Environment.NewLine;
            return
                $"The following comment was added: {this.Description}{Line}" +
                $"At >>> {this.Time}{Line}";
        }
    }
}
