﻿using System;
using System.Collections.Generic;
using System.Text;
using WIM.Models.Contracts;
using WIM.Core.Contracts;

namespace WIM.Models.Abstract
{
    public abstract class TeamElement : ITeamElement
    {
        
        protected  List<IWorkItem> workItems;
        protected  List<IActivityHistory> activityHistory;

        public TeamElement()
        {
            
            this.workItems = new List<IWorkItem>();
            this.activityHistory = new List<IActivityHistory>();
        }

        public IReadOnlyList<IWorkItem> WorkItems => this.workItems;

        public IReadOnlyList<IActivityHistory> ActivityHistory => this.activityHistory;

        
        public void AddItemToBoard(IWorkItem workItem)
        {
            this.workItems.Add(workItem);
        }

        public void AddActivityHistory(IActivityHistory activityHistory)
        {
            this.activityHistory.Add(activityHistory);
        }


    }
}
