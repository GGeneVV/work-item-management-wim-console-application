﻿using System;
using System.Collections.Generic;
using System.Text;
using WIM.Models.Contracts;
using WIM.Models.Units;
using WIM.Core.Contracts;

namespace WIM.Models.Abstract
{
    public abstract class WorkItem : IWorkItem
    {
        //Field
        private static int id = 1;
        protected string title;
        protected string description;
        protected List<Comment> comments;
        protected List<ActivityHistory> history;

        //Constructor..

        public WorkItem(string title, string description)
        {
            
            Id = id++;
            this.Title = title;
            this.Description = description;
            this.Comments = new List<Comment>();
            this.History = new List<ActivityHistory>();
        }
        //Must be unique....
        public int Id { get; set; }

        public string Title
        {
            get => this.title;
            set
            {
                if (string.IsNullOrEmpty(value) || value.Length < 10 || value.Length > 50)
                {
                    throw new Exception("Title should be between 10 and 50 symbols");
                }
                this.title = value;

                
            }

        }
        public string Description
        {
            get => this.description;
            set
            {
                if (string.IsNullOrEmpty(value) || value.Length<10 || value.Length>500)
                {
                    throw new Exception("Description should be between 10 and 500 symbols");
                }
                this.description = value;
            }
        }

        public List<Comment> Comments { get; set; }
     
        
        public List<ActivityHistory> History { get; set; }


        //Method
        public override string ToString()
        {
            var newLine = Environment.NewLine;
            return $"ID:{this.Id};" +
                $"{newLine}Title: {this.Title};" +
                $"{newLine}Description: {this.Description};{newLine}";
        }

    }

}

