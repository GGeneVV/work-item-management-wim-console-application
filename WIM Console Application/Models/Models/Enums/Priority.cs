﻿namespace WIM.Models.Enums
{
    public enum Priority
    {
        High,
        Medium,
        Low
    }
}
