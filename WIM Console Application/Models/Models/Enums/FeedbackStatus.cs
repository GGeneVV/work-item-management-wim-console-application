﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WIM.Models.Enums
{
    public enum FeedbackStatus
    {
        New,
        Unscheduled,
        Scheduled,
        Done
    }
}
