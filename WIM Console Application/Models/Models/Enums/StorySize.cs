﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WIM.Models.Enums
{
    public enum StorySize
    {
        Large,
        Medium,
        Small
    }
}
