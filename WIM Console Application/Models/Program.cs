﻿using System;
using WIM.Core;
using WIM.Models.Units;

namespace WIM

{
    class Program
    {
        static void Main(string[] args)
        {
            Engine.Instance.Run();
        }
    }
}
