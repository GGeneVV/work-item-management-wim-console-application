﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using WIM.Commands.CommandClasses;
using WIM.Core;
using WIM.Models.Enums;
using WIM.Models.Units;

namespace WIM_Tests.Units_Test.Story_Tests
{
    [TestClass]
    public class StoryShould : BaseTestClass
    {
        [TestMethod]
        public void CorrectlyAssignPassedValye()
        {
            //Arange
            string title = "storyTestTitle";
            string description = "descriptionOfStoryTest";
            StorySize size = StorySize.Large;
            Priority priority = Priority.High;
            StoryStatus status = StoryStatus.InProgress;

            //Act
            var sut = new Story(title, description, size, priority, status);

            //Assert           
            Assert.AreEqual(sut.Title, "storyTestTitle");
            Assert.AreEqual(sut.Description, "descriptionOfStoryTest");
            Assert.AreEqual(sut.Size, StorySize.Large);
            Assert.AreEqual(sut.Priority, Priority.High);
            Assert.AreEqual(sut.StoryStatus, StoryStatus.InProgress);
        }

        [TestMethod]
        public void Throw_Exception_When_TitleIsShorter()
        {
            //Arange
            string title = new string('a', 9);
            string description = "descriptionOfStoryTest";
            StorySize size = StorySize.Large;
            Priority priority = Priority.High;
            StoryStatus status = StoryStatus.InProgress;
            
            //Act
            var exception = Assert.ThrowsException<Exception>(() => new Story(title, description, size, priority, status));
            Assert.IsTrue(exception.Message.Contains("Title should be between 10 and 50 symbols"));
        }

        [TestMethod]
        public void Throw_Exception_When_TitleIsLonger()
        {
            string title = new string('t', 51);
            string description = "descriptionOfStoryTest";
            StorySize size = StorySize.Large;
            Priority priority = Priority.High;
            StoryStatus status = StoryStatus.InProgress;

            //Act
            var exception = Assert.ThrowsException<Exception>(() => new Story(title, description, size, priority, status));
            Assert.IsTrue(exception.Message.Contains("Title should be between 10 and 50 symbols"));
        }

        [TestMethod]
        public void Throw_Exception_Whem_TitleNull()
        {
            string title = null;
            string description = "descriptionOfStoryTest";
            StorySize size = StorySize.Large;
            Priority priority = Priority.High;
            StoryStatus status = StoryStatus.InProgress;

            //Act
            var exception = Assert.ThrowsException<Exception>(() => new Story(title, description, size, priority, status));
            Assert.IsTrue(exception.Message.Contains("Title should be between 10 and 50 symbols"));
        }

        [TestMethod]
        public void Throw_Exception_When_DescriptionIsShorter()
        {
            string title = "storyTestTitle";
            string description = new string('t', 9);
            StorySize size = StorySize.Large;
            Priority priority = Priority.High;
            StoryStatus status = StoryStatus.InProgress;

            var exception = Assert.ThrowsException<Exception>
                (() => new Story(title, description, size, priority, status));

            Assert.IsTrue(exception.Message.Contains("Description should be between 10 and 500 symbols"));
        }

        [TestMethod]
        public void Throw_Exception_When_DescriptionIsLonger()
        {
            string title = "storyTestTitle";
            string description = new string('t', 501);
            StorySize size = StorySize.Large;
            Priority priority = Priority.High;
            StoryStatus status = StoryStatus.InProgress;

            var exception = Assert.ThrowsException<Exception>
                (() => new Story(title, description, size, priority, status));
            Assert.IsTrue(exception.Message.Contains("Description should be between 10 and 500 symbols"));
        }

        [TestMethod]
        public void Throw_Exception_When_DescriptionIsNull()
        {
            string title = "storyTestTitle";
            string description = null;
            StorySize size = StorySize.Large;
            Priority priority = Priority.High;
            StoryStatus status = StoryStatus.InProgress;

            var exception = Assert.ThrowsException<Exception>(() => new Story(title, description, size, priority, status));
            Assert.IsTrue(exception.Message.Contains("Description should be between 10 and 500 symbols"));
        }

        [TestMethod]
        public void Return_correct_toString()
        {
            int id = 1;
            string title = "storyTestTitle";
            string description = "descriptionOfStoryTest";
            StorySize size = StorySize.Large;
            Priority priority = Priority.High;
            StoryStatus status = StoryStatus.InProgress;

           var story = new Story(title, description, size, priority, status);
            var newLine = Environment.NewLine;
            string actual = story.ToString();
            
            Assert.IsTrue(actual.Contains($"ID:{id};" +
                $"{newLine}Title: {title};" +
                $"{newLine}Description: {description};"));
           
        }

        [TestMethod]
        public void Correctly_Initialize_Collection()
        {
            string title = "storyTestTitle";
            string description = "descriptionOfStoryTest";
            StorySize size = StorySize.Large;
            Priority priority = Priority.High;
            StoryStatus status = StoryStatus.InProgress;

            var story = new Story(title, description, size, priority, status);

            Assert.IsTrue(story.Comments is List<Comment>);
            Assert.IsTrue(story.History is List<ActivityHistory>);
        }

        [TestMethod]
        public void CorrectlyAssignWorkItemStory_Should()
        {
            string storySize = "Large";
            string priority = "High";
            string storyStatus = "InProgress";

            var story = new CreateNewStoryCommand
                (
                new List<string>
                {
                    "story  test 001",
                    "description of story test 001",
                    storySize,
                    priority,
                    storyStatus,
                }
                );

            story.Execute();
            var member = new CreateNewPersonCommand(new List<string> { "MemberTestName" });
            member.Execute();

            IList<string> commandParameters = new List<string> { "1", "MemberTestName", "story" };
            //var expected = ;          
            var sut = new AssignWorkItem(commandParameters);

            Assert.IsNull(Database.Instance.Story[0].Assignee);
            sut.Execute();

            Assert.AreEqual(Database.Instance.Members[0].Name, Database.Instance.Story[0].Assignee.Name);
        }


    }
}