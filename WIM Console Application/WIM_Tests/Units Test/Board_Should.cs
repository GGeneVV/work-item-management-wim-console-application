﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using WIM.Commands.CommandClasses;
using WIM.Models.Enums;
using WIM.Models.Units;
using WIM.Core.Contracts;
namespace WIM_Tests
{
    [TestClass]
    public class Board_Should : BaseTestClass
    {
        [TestMethod]

        public void Assign_Correct_Name()
        {

            string boardName = "Board";

            Board board = new Board(boardName);

            Assert.AreEqual(board.Name, boardName);
           

        }
        [TestMethod]
        public void Throw_Argument_Exception_AtTitle()
        {

            string boardName = "BoardName is too long max is 10 symbols";

            var exception = Assert.ThrowsException<ArgumentException>(() => new Board(boardName));

            Assert.AreEqual(exception.Message , "The name must be between five and ten symbols.");
        }

        [TestMethod]
        public void Throws_Null_Exception_AtTitle()
        {

            string boardName = "min";
           


            var exception = Assert.ThrowsException<ArgumentException>(() => new Board(boardName));

            Assert.IsTrue(exception.Message.Contains("The name must be between five and ten symbols."));
        }
    }
}
