﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using WIM.Models.Enums;
using WIM.Models.Units;

namespace WIM_Tests.Units_Test
{
    [TestClass]
    public class Bug_Should :BaseTestClass
    {
        [TestMethod]
        public void Constructor_Should()
        {
            //Arrange
            string title = "bugTestTitle";
            string description = "BugDescriptionTest";
            BugStatus status = BugStatus.Active;
            Severity severity = Severity.Critical;
            Priority priority = Priority.High;
            string stepsToReproduce = "a";

            //Act
            var bug = new Bug(title, description, status, severity, priority, stepsToReproduce);

            //Assert
            Assert.AreEqual(bug.Title, title);
            Assert.AreEqual(bug.Description, description) ;
            Assert.AreEqual(bug.BugStatus, status);
            Assert.AreEqual(bug.Severity, severity);
            Assert.AreEqual(bug.Priority, priority);
            Assert.AreEqual(bug.StepsToReproduce, stepsToReproduce);
        }
                
        [TestMethod]
        public void Throw_Exception_When_TitleIsShorter()
        {
            //Arange
            string title = new string ('b', 9);
            string description = "BugDescriptionTest";
            BugStatus status = BugStatus.Active;
            Severity severity = Severity.Critical;
            Priority priority = Priority.High;
            string stepsToReproduce = "a";

            
            //Act
            var exception = Assert.ThrowsException<Exception>(() => new Bug(title, description, status, severity, priority, stepsToReproduce));
            Assert.IsTrue(exception.Message.Contains("Title should be between 10 and 50 symbols"));
        }

        [TestMethod]
        public void Throw_Exception_When_TitleIsLonger()
        {
            //Arrange
            string title = new string('b', 51);
            string description = "BugDescriptionTest";
            BugStatus status = BugStatus.Active;
            Severity severity = Severity.Critical;
            Priority priority = Priority.High;
            string stepsToReproduce = "a";


            //Act
            var exception = Assert.ThrowsException<Exception>(() => new Bug(title, description, status, severity, priority, stepsToReproduce));
            Assert.IsTrue(exception.Message.Contains("Title should be between 10 and 50 symbols"));
        }

        [TestMethod]
        public void Throw_Exception_Whem_TitleIsNull()
        {
            //Arrange
            string title = null;
            string description = "BugDescriptionTest";
            BugStatus status = BugStatus.Active;
            Severity severity = Severity.Critical;
            Priority priority = Priority.High;
            string stepsToReproduce = "a";


            //Act
            var exception = Assert.ThrowsException<Exception>(() => new Bug(title, description, status, severity, priority, stepsToReproduce));
            Assert.IsTrue(exception.Message.Contains("Title should be between 10 and 50 symbols"));
        }

        [TestMethod]
        public void Throw_Exception_When_DescriptionIsShorter()
        {
            //Arrange
            string title = "BugTestTitle";
            string description = new string('b', 9);
            BugStatus status = BugStatus.Active;
            Severity severity = Severity.Critical;
            Priority priority = Priority.High;
            string stepsToReproduce = "a";

            //Act
            var exception = Assert.ThrowsException<Exception>(() => new Bug(title, description, status, severity, priority, stepsToReproduce));

            Assert.IsTrue(exception.Message.Contains("Description should be between 10 and 500 symbols"));
        }

        [TestMethod]
        public void Throw_Exception_When_DescriptionIsLonger()
        {
            //Arrange
            string title = "BugTestTitle";
            string description = new string('b', 501);
            BugStatus status = BugStatus.Active;
            Severity severity = Severity.Critical;
            Priority priority = Priority.High;
            string stepsToReproduce = "a";

            //Act
            var exception = Assert.ThrowsException<Exception>(() => new Bug(title, description, status, severity, priority, stepsToReproduce));
            Assert.IsTrue(exception.Message.Contains("Description should be between 10 and 500 symbols"));
        }

        [TestMethod]
        public void Throw_Exception_When_DescriptionIsNull()
        {
            //Arrange
            string title = "BugTestTitle";
            string description = null;
            BugStatus status = BugStatus.Active;
            Severity severity = Severity.Critical;
            Priority priority = Priority.High;
            string stepsToReproduce = "a";

            //Act
            var exception = Assert.ThrowsException<Exception>(() => new Bug(title, description, status, severity, priority, stepsToReproduce));
            Assert.IsTrue(exception.Message.Contains("Description should be between 10 and 500 symbols"));
        }

        [TestMethod]
        public void Return_correct_toString()
        {
            int id = 1;
            string title = "BugTestTitle";
            string description = "BugDescriptionTest";
            BugStatus status = BugStatus.Active;
            Severity severity = Severity.Critical;
            Priority priority = Priority.High;
            string stepsToReproduce = "a";


            var bug = new Bug(title, description, status, severity, priority, stepsToReproduce);
            var newLine = Environment.NewLine;
         
            Assert.AreEqual(
                $"ID:{id};" +
                $"{newLine}Title: {title};" +
                $"{newLine}Description: {description};" +
                $"{newLine}Bug with {status} status , {priority} and {severity}" +
                $"{newLine}Not assigned to Team/member!" +
                $"{newLine}Please follow these steps to reporoduce the Bug:" +
                newLine + stepsToReproduce,
                bug.ToString()
                );
        }

        [TestMethod]
        public void Correctly_Initialize_Collection()
        {
            string title = "BugTestTitle";
            string description = "BugDescriptionTest";
            BugStatus status = BugStatus.Active;
            Severity severity = Severity.Critical;
            Priority priority = Priority.High;
            string stepsToReproduce = "a";


            var bug = new Bug(title, description, status, severity, priority, stepsToReproduce);

            Assert.IsTrue(bug.Comments is List<Comment>);
            Assert.IsTrue(bug.History is List<ActivityHistory>);
        }       
    }
}
