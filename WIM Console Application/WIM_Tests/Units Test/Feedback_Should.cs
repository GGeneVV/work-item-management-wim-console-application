﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using WIM.Commands.CommandClasses;
using WIM.Models.Enums;
using WIM.Models.Units;
using WIM.Core.Contracts;

namespace WIM_Tests
{
    [TestClass]
    public class Feedback_Should : BaseTestClass
    {
        [TestMethod]

        public void Assign_Correct_Values()
        {

            int feedbackRating = 5;
            FeedbackStatus status = 0;

            Feedback feedback = new Feedback("FTitleAtLeast10", "FDescription", feedbackRating, status);

            Assert.AreEqual(feedback.Rating, feedbackRating);
            Assert.AreEqual(feedback.FeedbackStatus, status);

        }

        [TestMethod]
        public void Throw_Exception_AtTitle()
        {

            int feedbackRating = 11;
            FeedbackStatus status = 0;

          

            var exception = Assert.ThrowsException<Exception>(() => new Feedback("FTitle", "FDescription", feedbackRating, status));

            Assert.IsTrue(exception.Message.Contains("Title should be between 10 and 50 symbols"));
        }
        [TestMethod]
        public void Throws_Null_Exception_AtTitle()
        {

            int feedbackRating = 11;
            FeedbackStatus status = 0;



            var exception = Assert.ThrowsException<Exception>(() => new Feedback(null, "FDescription", feedbackRating, status));

            Assert.IsTrue(exception.Message.Contains("Title should be between 10 and 50 symbols"));
        }

    }
}
