﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using WIM.Models.Units;

namespace WIM_Tests.Units_Test
{
    [TestClass]
    public class Comment_Should : BaseTestClass
    {
        [TestMethod]
        public void CorrectlyAssignPassedValye()
        {
            string autor = "Gosho";
            string description = "Description of test comment";
            DateTime dateTime = DateTime.Now;

            var sut = new Comment(autor, description);

            Assert.AreEqual(sut.Author, autor);
            Assert.AreEqual(sut.Description, description);
            //Assert.AreEqual(sut.Time, dateTime);
        }
    }
}
