using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using WIM.Models.Units;


namespace WIM_Tests
{
    [TestClass]
    public class TeamModel_Should
    {
        [TestMethod]
        public void Correctly_Assign_NameValue()
        {
            //Arrange expected
            string name = "Bug Bus";
            Team team = new Team(name);
            
            //Act...
            string teamName = team.Name;
            
            //Assert...
            Assert.AreEqual(teamName, name);

        }
        [TestMethod]
        public void Throw_Argument_Null_Exception_Whitespace()
        {
            //Arrange..

            string teamName = new string(' ' , 7);

            //Act...

            var exception = Assert.ThrowsException<ArgumentNullException>(() => new Team(teamName));
            ////Arrange

            Assert.IsTrue(exception.Message.Contains("Team name can not be null or empty"));

        }
        [TestMethod]
        public void Throw_Argument_Null_Exception_NullPassed()
        {
            //Arrange..

            string teamName = string.Empty;

            //Act...

            var exception = Assert.ThrowsException<ArgumentNullException>(() => new Team(teamName));
            ////Arrange

            Assert.IsTrue(exception.Message.Contains("Team name can not be null or empty"));

        }

        [TestMethod]
        public void Throw_Argument_Out_Of_Range_Short()
        {
            //Arrange..
            string name = "Bug";
            

            //Act...
           
           var exception =  Assert.ThrowsException<ArgumentException>(()=>new Team(name));
            ////Arrange
          
            Assert.IsTrue(exception.Message.Contains("The name must be between five and ten symbols."));
            
        }

        [TestMethod]
        public void Throw_Argument_Out_Of_Range_Long()
        {
            //Arrange..
            string name = "Bug Busters is tooo long";


            //Act...

            var exception = Assert.ThrowsException<ArgumentException>(() => new Team(name));
            ////Arrange

            Assert.IsTrue(exception.Message.Contains("The name must be between five and ten symbols."));

        }
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void Throws_ArgumentException()
        {

            var member = new Team(null);

        }
    }
}
