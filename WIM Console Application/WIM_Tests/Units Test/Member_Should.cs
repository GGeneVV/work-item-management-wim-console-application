﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using WIM.Models.Units;

namespace WIM_Tests
{
    [TestClass]
    public class Member_Should
    {
        [TestMethod]
        public void Assign_ValidName()
        {
            //expected
            var name = "Bug Busters";


            Member member = new Member(name);

            //actual
            string actualName = member.Name;

            Assert.AreEqual(actualName, name);

        }
        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void Throws_ArgumentException()
        {

            var member = new Member(null);

        }
    }
}
