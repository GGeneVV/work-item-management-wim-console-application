﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using WIM.Models.Units;

namespace WIM_Tests.Units_Test
{
    [TestClass]
    public class ActivityHistory_Should
    {
        [TestMethod]
        public void CorectluCorrectlyAssignPassedValye()
        {
            //Arrange
            string description = "ActivityHistoryDescription";
            DateTime dateTime = DateTime.Now;

            //Act

            var sut = new ActivityHistory(description);
            //Assert
            Assert.AreEqual(sut.Description, description);
            //Assert.AreEqual(sut.Description, description);
        }



        [TestMethod]
        public void Throw_Exception_When_DescriptionIsShorter()
        {
            //Arrange
            string description = new string('b', 9);
                      
            //Act
            var exception = Assert.ThrowsException<ArgumentOutOfRangeException>(() => new ActivityHistory(description));

            Assert.IsTrue(exception.Message.Contains("The activity history description must be between 10 and 500 symbols"));
        }

        [TestMethod]
        public void Throw_Exception_When_DescriptionIsLonger()
        {
            //Arrange          
            string description = new string('b', 501);

            //Act
            var exception = Assert.ThrowsException<ArgumentOutOfRangeException>(() => new ActivityHistory(description));

            //Assert
            Assert.IsTrue(exception.Message.Contains("The activity history description must be between 10 and 500 symbols"));
        }

        [TestMethod]
        public void Throw_Exception_When_DescriptionIsNull()
        {
            //Arrange
            
            string description = null;

            //Act
            var exception = Assert.ThrowsException<ArgumentNullException>(() => new ActivityHistory(description));

            //Assert
            Assert.IsTrue(exception.Message.Contains("Must have description"));              
        }
    }
}
