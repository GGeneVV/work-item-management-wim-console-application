﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;
using WIM.Core;
using WIM.Core.Contracts;
using WIM.Models.Abstract;
using WIM.Models.Units;

namespace WIM_Tests
{
    public class BaseTestClass
    {
        protected IDatabase database = Database.Instance;
        protected IFactory factory = Factory.Instance;
      
        [TestInitialize]
        public void ResetDatabaseAndId()
        {
            var instanceField = typeof(Database).GetField("instance", BindingFlags.NonPublic | BindingFlags.Static);
            var newDatabase = new Database();
            database = newDatabase;
            instanceField.SetValue(newDatabase, newDatabase);

            var idField = typeof(WorkItem).GetField("id", BindingFlags.NonPublic | BindingFlags.Static);
            var newId = 1;
            var workItem = new Bug("ResetIdTitle", "ResertIdDisctiptionBug", default, default, default, "ResetId");
            idField.SetValue(workItem, newId);
        }
        //protected IAcademyFactory factory = AcademyFactory.Instance;
        //protected IEngine engine = Engine.Instance;

        //[TestInitialize]
        //public void ResetEngine()
        //{
        //    Core.Engine.Instance.Seasons.Clear();
        //    Core.Engine.Instance.Students.Clear();
        //    Core.Engine.Instance.Trainers.Clear();
        //}
        //For not repeating it in every CommandTestMethod

        //[TestInitialize]
        //public void ClearEngine_Must()
        //{
        //    //This clear our engine lists for the up-coming test methods
        //    Engine.Instance..Clear();
        //    Engine.Instance.Students.Clear();
        //    Engine.Instance.Trainers.Clear();
        //}

    }
}
