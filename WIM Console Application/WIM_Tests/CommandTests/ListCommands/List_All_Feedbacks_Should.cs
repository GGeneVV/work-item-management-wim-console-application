﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using WIM.Commands.CommandClasses;
using WIM.Models.Enums;
using WIM.Models.Units;

namespace WIM_Tests.CommandTests.ListCommands
{
    [TestClass]
    public class List_All_Feedbacks_Should : BaseTestClass
    {
        [TestMethod]

        public void Return_All_Feedbacks()
        {


            Feedback feedback = new Feedback("Feedback One Title", "Feedback One Description", 5 , FeedbackStatus.Unscheduled);
            Feedback feedbackTwo = new Feedback("Feedback Two Title", "Feedback Two Description", 5 , FeedbackStatus.Unscheduled);
            Feedback feedbackThree = new Feedback("Feedback Three Title", "Feedback Three Description", 5 , FeedbackStatus.Unscheduled);

            database.Feedback.Add(feedback);
            database.Feedback.Add(feedbackTwo);
            database.Feedback.Add(feedbackThree);


            List<string> command = new List<string> { "listallfeedbacks" };
            ListAllFeedbacksCommand list = new ListAllFeedbacksCommand(command);

            var output = list.Execute();


            Assert.IsTrue(output.Contains("Feedback One Title"));
            Assert.IsTrue(output.Contains("Feedback One Description"));
            Assert.IsTrue(output.Contains("5"));
            Assert.IsTrue(output.Contains("Unscheduled"));

            Assert.IsTrue(output.Contains("Feedback Two Title"));
            Assert.IsTrue(output.Contains("Feedback Two Description"));
            Assert.IsTrue(output.Contains("5"));
            Assert.IsTrue(output.Contains("Unscheduled"));

            Assert.IsTrue(output.Contains("Feedback Three Title"));
            Assert.IsTrue(output.Contains("Feedback Three Description"));
            Assert.IsTrue(output.Contains("5"));
            Assert.IsTrue(output.Contains("Unscheduled"));
        }

        [TestMethod]

        public void Return_No_Feedbacks()
        {


            List<string> command = new List<string> {"listallfeedbacks"};
            ListAllFeedbacksCommand list = new ListAllFeedbacksCommand(command);

            var output = list.Execute();

      

            Assert.IsTrue(output.Contains("There is no feedbacks to show!"));
           
        }
    }
}
