﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using WIM.Commands.CommandClasses;
using WIM.Models.Enums;
using WIM.Models.Units;

namespace WIM_Tests.CommandTests.ListCommands
{
    [TestClass]
    public class List_All_Bugs_Should:BaseTestClass
    {
        [TestMethod]

        public void Return_All_Bugs()
        {
            Bug bugOne = new Bug("BugTitleAtLeast1", "Bug Description", BugStatus.Active, Severity.Minor, Priority.Low, stepsToReproduce: "Debug and find out whats happening");
            Bug bugTwo = new Bug("BugTitleAtLeast2", "Bug Description", BugStatus.Active, Severity.Minor, Priority.Low, stepsToReproduce: "Debug and find out whats happening");
            Bug bugThree = new Bug("BugTitleAtLeast3", "Bug Description", BugStatus.Active, Severity.Minor, Priority.Low, stepsToReproduce: "Debug and find out whats happening");

           
            database.AddBug(bugOne);
            database.AddBug(bugTwo);
            database.AddBug(bugThree);


            List<string> command =new List<string> { "listallbugs" };
            ListAllBugsCommand list = new ListAllBugsCommand(command);
            var output = list.Execute();
          
            

            Assert.IsTrue(output.Contains("BugTitleAtLeast1"));
            Assert.IsTrue(output.Contains("BugTitleAtLeast2"));
            Assert.IsTrue(output.Contains("BugTitleAtLeast3"));
        }

        [TestMethod]
        public void Return_No_Bugs()
        {


            List<string> command = new List<string> { "listallfeedbacks" };
            ListAllBugsCommand list = new ListAllBugsCommand(command);

            var output = list.Execute();



            Assert.IsTrue(output.Contains("There is no bugs to show!"));

        }
    }
}
