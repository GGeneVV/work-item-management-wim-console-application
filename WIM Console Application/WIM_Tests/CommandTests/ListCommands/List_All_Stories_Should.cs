﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using WIM.Commands.CommandClasses;
using WIM.Models.Enums;
using WIM.Models.Units;

namespace WIM_Tests.CommandTests.ListCommands
{
    [TestClass]
    public class List_All_Stories_Should : BaseTestClass
    {
        [TestMethod]

        public void Return_All_Bugs()
        {

            Story storyOne = new Story("StoryTitleOne", "Story One Description",StorySize.Large , Priority.High,StoryStatus.NotDone);
            Story storyTwo = new Story("StoryTitleTwo", "Story Two Description",StorySize.Medium , Priority.High,StoryStatus.NotDone);
            Story storyThree = new Story("StoryTitleThree", "Story Three Description",StorySize.Small , Priority.High,StoryStatus.NotDone);
           
            database.Story.Add(storyOne);
            database.Story.Add(storyTwo);
            database.Story.Add(storyThree);


            List<string> command = new List<string> { "listallstories" };
            ListAllStoriesCommand list = new ListAllStoriesCommand(command);
            var output = list.Execute();



            Assert.IsTrue(output.Contains("StoryTitleOne"));
            Assert.IsTrue(output.Contains("Story One Description"));

            Assert.IsTrue(output.Contains("StoryTitleTwo"));
            Assert.IsTrue(output.Contains("Story Two Description")); 
            
            Assert.IsTrue(output.Contains("StoryTitleThree"));
            Assert.IsTrue(output.Contains("Story Three Description"));
        }

        [TestMethod]
        public void Return_No_Stories()
        {


            List<string> command = new List<string> { "listallfeedbacks" };
            ListAllStoriesCommand list = new ListAllStoriesCommand(command);

            var output = list.Execute();

            Assert.IsTrue(output.Contains("There is no stories to show!"));

        }
    }
}
