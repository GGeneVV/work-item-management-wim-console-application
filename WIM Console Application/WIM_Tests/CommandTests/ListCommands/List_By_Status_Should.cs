﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using WIM.Commands.CommandClasses;
using WIM.Models.Enums;
using WIM.Models.Units;

namespace WIM_Tests.CommandTests.ListCommands
{
    [TestClass]
    public class List_By_Status_Should : BaseTestClass
    {
        [TestMethod]

        public void Return_AllBugs_With_Given_Status()
        {
            Bug bugOne = new Bug("BugActiveStatusOne", "Bug Description", BugStatus.Active, Severity.Minor,
                Priority.Low,
                stepsToReproduce: "Debug and find out whats happening");
            Bug bugTwo = new Bug("BugActiveStatusTwo", "Bug Description", BugStatus.Active, Severity.Minor,
                Priority.Low, stepsToReproduce: "Debug and find out whats happening");
            Bug bugThree = new Bug("BugDiffStatus", "Bug Description", BugStatus.Fixed, Severity.Minor, Priority.Low,
                stepsToReproduce: "Debug and find out whats happening");


            database.AddBug(bugOne);
            database.AddBug(bugTwo);
            database.AddBug(bugThree);


            List<string> command = new List<string> {"Active"};
            ListByStatusCommand list = new ListByStatusCommand(command);
            var output = list.Execute();



            Assert.IsTrue(output.Contains("BugActiveStatusOne"));
            Assert.IsTrue(output.Contains("BugActiveStatusTwo"));

            Assert.IsTrue(output.Contains("Active"));


            Assert.IsFalse(output.Contains("BugDiffStatus"));
            Assert.IsFalse(output.Contains("Fixed"));
        }

        [TestMethod]

        public void Return_AllStories_With_Given_Status()
        {
            Story storyOne = new Story("StoryDoneOne","Story One Description" ,StorySize.Small,Priority.Low,StoryStatus.Done);
            Story storyTwo = new Story("StoryDoneTwo","Story Two Description" ,StorySize.Small,Priority.Low,StoryStatus.Done);
            Story storyThree = new Story("StoryNotDoneThree","Story Three Description" ,StorySize.Small,Priority.Low,StoryStatus.NotDone);
           


            database.Story.Add(storyOne);
            database.Story.Add(storyTwo);
            database.Story.Add(storyThree);


            List<string> command = new List<string> { "Done" };
            ListByStatusCommand list = new ListByStatusCommand(command);
            var output = list.Execute();



            Assert.IsTrue(output.Contains("StoryDoneOne"));
            Assert.IsTrue(output.Contains("StoryDoneTwo"));

            Assert.IsTrue(output.Contains("Done"));


            Assert.IsFalse(output.Contains("StoryNotDoneThree"));
            Assert.IsFalse(output.Contains("NotDone"));
        }

        [TestMethod]

        public void Return_AllFeedback_With_Given_Status()
        {
            Feedback feedbackOne = new Feedback("FeedbackTitle One Done","FeedbackDescription",9,FeedbackStatus.Done);
            Feedback feedbackTwo = new Feedback("FeedbackTitle Two Done","FeedbackDescription",9,FeedbackStatus.Done);
            Feedback feedbackThree = new Feedback("FeedbackTitle Three Scheduled","FeedbackDescription",9,FeedbackStatus.Scheduled);
           

            database.Feedback.Add(feedbackOne);
            database.Feedback.Add(feedbackTwo);
            database.Feedback.Add(feedbackThree);


            List<string> command = new List<string> { "Done" };
            ListByStatusCommand list = new ListByStatusCommand(command);
            var output = list.Execute();



            Assert.IsTrue(output.Contains("FeedbackTitle One Done"));
            Assert.IsTrue(output.Contains("FeedbackTitle Two Done"));

            Assert.IsTrue(output.Contains("Done"));


            Assert.IsFalse(output.Contains("FeedbackTitle Three Scheduled"));
            Assert.IsFalse(output.Contains("Scheduled"));
        }
    }
}