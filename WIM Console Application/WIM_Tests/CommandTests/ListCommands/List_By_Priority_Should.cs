﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using WIM.Commands.CommandClasses;
using WIM.Models.Enums;
using WIM.Models.Units;

namespace WIM_Tests.CommandTests.ListCommands
{
    [TestClass]
    public class List_By_Priority_Should : BaseTestClass
    {
        [TestMethod]

        public void Return_Where_Given_Priority_Match()
        {

            Story storyOne = new Story("StoryTitleOne", "Story One Description", StorySize.Large, Priority.Low, StoryStatus.NotDone);
            Story storyTwo = new Story("StoryTitleTwo", "Story Two Description", StorySize.Medium, Priority.Medium, StoryStatus.NotDone);
            Story storyThree = new Story("Story Three Title", "Story Three Description", StorySize.Small, Priority.High, StoryStatus.NotDone);

            database.Story.Add(storyOne);
            database.Story.Add(storyTwo);
            database.Story.Add(storyThree);


            List<string> command = new List<string> { "High" };
            ListByPriorityCommand list = new ListByPriorityCommand(command);
            var output = list.Execute();



            Assert.IsTrue(output.Contains("Story Three Title"));
            Assert.IsTrue(output.Contains("Story Three Description"));
            
           Assert.IsFalse(output.Contains("StoryTitleTwo"));
            Assert.IsFalse(output.Contains("Story Two Description"));

            Assert.IsFalse(output.Contains("StoryTitleOne"));
            Assert.IsFalse(output.Contains("Story One Description"));
        }

        [TestMethod]
        public void Return_No_Stories()
        {
            Story storyOne = new Story("StoryTitleOne", "Story One Description", StorySize.Large, Priority.Low, StoryStatus.NotDone);

            List<string> command = new List<string> { "listallfeedbacks" };
            ListByPriorityCommand list = new ListByPriorityCommand(command);
            database.Story.Add(storyOne);
            var output = list.Execute();

            Assert.IsFalse(output.Contains("There is no stories to show!"));

        }
    }
}