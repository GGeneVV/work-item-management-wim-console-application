﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using WIM.Commands.CommandClasses;
using WIM.Models.Enums;
using WIM.Models.Units;

namespace WIM_Tests.CommandTests.ListCommands
{
    [TestClass]
    public class List_By_Rating_Should : BaseTestClass
    {
        [TestMethod]

        public void Return_Feedbacks_With_Given_Rating()
        {


            Feedback feedbackOne = new Feedback("Feedback Two Title", "Feedback Ex Description", 6, FeedbackStatus.Unscheduled);
            Feedback feedbackTwo = new Feedback("Feedback One Title", "Feedback One Description", 5, FeedbackStatus.Unscheduled);

            database.Feedback.Add((feedbackTwo));
            database.Feedback.Add(feedbackOne);


            List<string> command = new List<string> { "anything" };
            ListByRatingCommand list = new ListByRatingCommand(command);

            var output = list.Execute();

            //
            Assert.AreEqual(feedbackTwo.Rating, 5);
            Assert.AreEqual(feedbackOne.Rating, 6);
            Assert.IsTrue(output.Contains("Feedback One Title"));
            Assert.IsTrue(output.Contains("Feedback Two Title"));

        }

        [TestMethod]

        public void Throws_Exception_When_Rating_Out_Of_Range()
        {

            int rating = 25;
            var exception = Assert.ThrowsException<ArgumentOutOfRangeException>(() => new Feedback("Feedback One Title",
                 "Feedback One Description", rating, FeedbackStatus.Unscheduled));


           
            Assert.IsFalse(exception.Message.Contains(" Specified argument was out of the range of valid values."));
            List<string> command = new List<string> { "listallfeedbacks" };
            ListByRatingCommand list = new ListByRatingCommand(command);

          

        }
    }
}