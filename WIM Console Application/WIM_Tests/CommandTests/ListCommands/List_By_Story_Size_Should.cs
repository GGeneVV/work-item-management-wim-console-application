﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using WIM.Commands.CommandClasses;
using WIM.Models.Enums;
using WIM.Models.Units;

namespace WIM_Tests.CommandTests.ListCommands
{
    [TestClass]
    public class List_By_Story_Size_Should : BaseTestClass
    {
        [TestMethod]

        public void Return_All_Stories_With_Given_Size()
        {

            Story storyOne = new Story("StoryTitleOne", "Story One Description", StorySize.Large, Priority.High,
                StoryStatus.NotDone);
            Story storyTwo = new Story("StoryTitleTwo", "Story Two Description", StorySize.Medium, Priority.High,
                StoryStatus.NotDone);
            Story storyThree = new Story("StoryTitleThree", "Story Three Description", StorySize.Small, Priority.High,
                StoryStatus.NotDone);

            database.Story.Add(storyOne);
            database.Story.Add(storyTwo);
            database.Story.Add(storyThree);


            List<string> command = new List<string> {"Large"};
            ListBySizeCommand list = new ListBySizeCommand(command);
            var output = list.Execute();



            Assert.IsTrue(output.Contains("Large"));
            Assert.IsTrue(output.Contains("StoryTitleOne"));

            Assert.IsFalse(output.Contains("Medium"));
            Assert.IsFalse(output.Contains("StoryTitleTwo"));

            Assert.IsFalse(output.Contains("Small"));
            Assert.IsFalse(output.Contains("StoryTitleThree"));
        }

    }
}