﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using WIM.Commands.CommandClasses;
using WIM.Models.Enums;
using WIM.Models.Units;

namespace WIM_Tests.CommandTests.ListCommands
{
    [TestClass]
    public class List_By_Severity_Should : BaseTestClass
    {



        [TestMethod]

        public void return_where_given_severity_match()
        {

            Bug bugone = new Bug("bugtitleatleast1", "bug description", BugStatus.Active, Severity.Critical, Priority.Low, stepsToReproduce: "debug and find out whats happening");
            Bug bugtwo = new Bug("bugtitleatleast2", "bug description", BugStatus.Active, Severity.Major, Priority.Low, stepsToReproduce: "debug and find out whats happening");
            Bug bugthree = new Bug("bugtitleatleast3", "bug description", BugStatus.Active, Severity.Minor, Priority.Low, stepsToReproduce: "debug and find out whats happening");



            database.Bug.Add(bugone);
            database.Bug.Add(bugtwo);
            database.Bug.Add(bugthree);


            List<string> command = new List<string> { "critical" };
            ListBySeverityCommand list = new ListBySeverityCommand(command);
            var output = list.Execute();



            Assert.IsTrue(output.Contains("Critical")); // Contains only bug with critical Severity
            Assert.IsFalse(output.Contains("Major")); // Not Contained..
            Assert.IsFalse(output.Contains("Minor")); // Not Contained..
                                                      
                                                      
            Assert.IsTrue(output.Contains("ID:1"));
            Assert.IsFalse(output.Contains("ID:2"));
            Assert.IsFalse(output.Contains("ID:3")); //Double check that its not in the list..

        }


    }
}