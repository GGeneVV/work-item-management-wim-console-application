﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using WIM.Commands.CommandClasses;
using WIM.Models.Enums;
using WIM.Models.Units;

namespace WIM_Tests.CommandTests.ListCommands
{
    [TestClass]
    public class List_ByTitle_Should :BaseTestClass
    {
        [TestMethod]

        public  void List_All_WorkItems_Based_GivenName()
        {
            //Arrange

            Bug bug = new Bug("Bug Title ToList" , "Some Description" , BugStatus.Active, Severity.Minor,Priority.Low , "Some steps to reproduce here");
            Story story = new Story("Valid Story Title", "Valid story description", StorySize.Large, Priority.Low, StoryStatus.Done);
            Feedback feedback = new Feedback("Feedback Title" , "Feedback Description", 5 ,FeedbackStatus.Done);

            database.Bug.Add(bug);
            database.Story.Add(story);
            database.Feedback.Add(feedback);

            List<string> commandParam = new List<string> {"all"};
            ListByTitleCommand listByTitle = new ListByTitleCommand(commandParam);

            var output = listByTitle.Execute();

            Assert.IsTrue(output.Contains("Bug Title ToList"));
            Assert.IsTrue(output.Contains("Valid Story Title"));
            Assert.IsTrue(output.Contains("Feedback Title"));

            //"There is no bugs to show!"
        }

        [TestMethod]

        public void Wont_Show_Stories_IfNoneMatching()
        {
            //Arrange

           
            Story story = new Story("Valid Story Title", "Valid story description", StorySize.Large, Priority.Low, StoryStatus.Done);
            
            //Command is not working as intended -imo it wont cover the case when story is added 
            //and database.Story.Count is not 0....the name desnt match but it wont return "No bugs to show"
            //it only works if there are no stories.
            //database.Story.Add(story);
           

            List<string> commandParam = new List<string> { "story" };
            ListByTitleCommand listByTitle = new ListByTitleCommand(commandParam);

            var output = listByTitle.Execute();

           
            Assert.IsTrue(output.Contains("There is no stories to show!"));
            
           

            
        }

    }
}
