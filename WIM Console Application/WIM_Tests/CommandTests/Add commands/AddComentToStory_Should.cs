﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using WIM.Commands.CommandClasses;
using WIM.Core;
using WIM.Models.Enums;
using WIM.Models.Units;

namespace WIM_Tests.CommandTests.Add_commands
{
    [TestClass]
    public class AddComentToStory_Should :BaseTestClass
    {
        [TestMethod]
        public void Correctly_Add_CommentToStory()
        {
            //Arrange
            string title = "storyTestTitle";
            string description = "descriptionOfStoryTest";
  
            string size = "Large";
            string priority = "High";
            string status = "InProgress";

            string iD = "1";
            string autorOfComment = "Gosho";
            string commentToAdd = "testComment";
            List<string> commandParameters = new List<string> { iD, autorOfComment, commentToAdd };
            //Act
            
            var st = new CreateNewStoryCommand(new List<string> {title, description, size, priority, status });
            st.Execute();
            var story = database.Story.First(x => x.Id == 1);

            var sut = new AddCommentToStoryCommand(commandParameters);
            sut.Execute();
            Assert.IsTrue(story.Comments != null);
        }
    }
}
