﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using WIM.Commands.CommandClasses;
using WIM.Core;
using WIM.Models.Contracts;
using WIM.Models.Enums;
using WIM.Models.Units;

namespace WIM_Tests.CommandTests.Add_commands
{
    [TestClass]
    public class AddCommentToBugComand_Should : BaseTestClass
    {
        [TestMethod]
        public void CorrectlyAddCommentToBugComand_()
        {
            //Arrange
            string iD = "1";
            string author = "testAutroPesho";
            string comentText = "Test comentar";

            string bugTitle = "ltest byg001";
            string description = "description of test bug001";
            BugStatus bugStatus = BugStatus.Active;
            Severity severity = Severity.Critical;
            Priority priority = Priority.High;
            string stepsToReproduce = "test steps to reproduce 001";

            var bug = new Bug
            (
                bugTitle,
                description,
                bugStatus,
                severity,
                priority,
                stepsToReproduce
            );
            database.Bug.Add(bug);

            List<string> parameters = new List<string>
            {
                iD,
                author,
                comentText
            };
            var addCommentToBug = new AddCommentToBugCommand(parameters);
            var expected = addCommentToBug.Execute();

            Assert.IsTrue(expected.Contains($"{author} added the following comment to bug: {comentText}"));

        }

        [TestMethod]
        public void Throw_Exception_NonIntId()
        {
            //Arrange
            string iD = "asd";
            string author = "testAutroPesho";
            string comentText = "Test comentar";

            string bugTitle = "ltest byg001";
            string description = "description of test bug001";
            BugStatus bugStatus = BugStatus.Active;
            Severity severity = Severity.Critical;
            Priority priority = Priority.High;
            string stepsToReproduce = "test steps to reproduce 001";

            var bug = new Bug
                (
                bugTitle,
                description,
                bugStatus,
                severity,
                priority,
                stepsToReproduce
                );
            database.Bug.Add(bug);

            List<string> parameters = new List<string>
            {
                iD,
                author,
                comentText
            };

            var addCommentToBug = new AddCommentToBugCommand(parameters);

            var exception = Assert.ThrowsException<ArgumentException>(() => addCommentToBug.Execute());
            Assert.IsTrue(exception.Message.Contains("Failed to parse \"Add Comment to Work Item\" command parameters."));
        }

        [TestMethod]
        public void Throw_Exception_IdOutOfRange()
        {
            //Arrange
            string iD1 = "22";
            string iD2 = "0";
            string author = "testAutroPesho";
            string comentText = "Test comentar";

            string bugTitle = "ltest byg001";
            string description = "description of test bug001";
            BugStatus bugStatus = BugStatus.Active;
            Severity severity = Severity.Critical;
            Priority priority = Priority.High;
            string stepsToReproduce = "test steps to reproduce 001";

            var bug = new Bug
                (
                bugTitle,
                description,
                bugStatus,
                severity,
                priority,
                stepsToReproduce
                );
            database.Bug.Add(bug);

            List<string> parameters1 = new List<string>
            {
                iD1,
                author,
                comentText
            };
            List<string> parameters2 = new List<string>
            {
                iD1,
                author,
                comentText
            };

            var addCommentToBug1 = new AddCommentToBugCommand(parameters1);
            var addCommentToBug2 = new AddCommentToBugCommand(parameters2);

            var exception1 = Assert.ThrowsException<ArgumentException>(() => addCommentToBug1.Execute());
            var exception2 = Assert.ThrowsException<ArgumentException>(() => addCommentToBug2.Execute());
            Assert.IsTrue(exception1.Message.Contains("Failed to parse \"Add Comment to Work Item\" command parameters."));
            Assert.IsTrue(exception2.Message.Contains("Failed to parse \"Add Comment to Work Item\" command parameters."));
        }

        [TestMethod]
        public void Throw_Exception_NullAuthor()
        {
            //Arrange
            string iD = "1";
            string author = null;
            string comentText = "Test comentar";

            string bugTitle = "ltest byg001";
            string description = "description of test bug001";
            BugStatus bugStatus = BugStatus.Active;
            Severity severity = Severity.Critical;
            Priority priority = Priority.High;
            string stepsToReproduce = "test steps to reproduce 001";

            var bug = new Bug
                (
                bugTitle,
                description,
                bugStatus,
                severity,
                priority,
                stepsToReproduce
                );
            database.Bug.Add(bug);

            List<string> parameters = new List<string>
            {
                iD,
                author,
                comentText
            };

            var addCommentToBug = new AddCommentToBugCommand(parameters);

            var exception = Assert.ThrowsException<ArgumentException>(() => addCommentToBug.Execute());
            Assert.IsTrue(exception.Message.Contains("Failed to parse \"Add Comment to Work Item\" command parameters."));
        }

        [TestMethod]
        public void Throw_Exception_NullComment()
        {
            //Arrange
            string iD = "1";
            string author = "testAutroPesho";
            string comentText = null;

            string bugTitle = "ltest byg001";
            string description = "description of test bug001";
            BugStatus bugStatus = BugStatus.Active;
            Severity severity = Severity.Critical;
            Priority priority = Priority.High;
            string stepsToReproduce = "test steps to reproduce 001";

            var bug = new Bug
                (
                bugTitle,
                description,
                bugStatus,
                severity,
                priority,
                stepsToReproduce
                );
            database.Bug.Add(bug);

            List<string> parameters = new List<string>
            {
                iD,
                author,
                comentText
            };

            var addCommentToBug = new AddCommentToBugCommand(parameters);

            var exception = Assert.ThrowsException<ArgumentException>(() => addCommentToBug.Execute());
            Assert.IsTrue(exception.Message.Contains("Failed to parse \"Add Comment to Work Item\" command parameters."));

        }
    }
}

