﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using WIM.Commands.CommandClasses;
using WIM.Commands.Contracts;
using WIM.Core;
using WIM.Models.Enums;
using WIM.Models.Units;
using WIM_Tests;

namespace UnitTests.UnitTests
{
    [TestClass]
    public class AssignWorkItemToMemberCommand_Should : BaseTestClass
    {
        [TestMethod]

        public void CorrectlyAssignWorkItemBug_Should()
        {
            string bugStatus = "Active";
            string severity = "Critical";
            string priority = "High";

            var bug = new CreateBugCommand
                (
                new List<string>
                {
                    "test byg001",
                    "description of test bug001",
                    bugStatus,
                    severity, priority,
                    "steps to reproduce test bug"
                }
                );
            bug.Execute();
            var member = new CreateNewPersonCommand(new List<string> { "MemberTestName" });
            member.Execute();           

            IList<string> commandParameters = new List<string> { "1", "MemberTestName", "bug" };
            //var expected = ;          
            var sut = new AssignWorkItem(commandParameters);
           
            Assert.IsNull(Database.Instance.Bug[0].Assignee);  
            sut.Execute();

            Assert.AreEqual(Database.Instance.Members[0].Name, Database.Instance.Bug[0].Assignee.Name);
        }

        [TestMethod]
        public void CorrectlyAssignWorkItemStory_Should()
        {
            string storySize = "Large";
            string priority = "High";
            string storyStatus = "InProgress";
           
            var story = new CreateNewStoryCommand
                (
                new List<string>
                {  
                    "story  test 001",
                    "description of story test 001",
                    storySize,
                    priority,
                    storyStatus,                    
                }
                );

            story.Execute();
            var member = new CreateNewPersonCommand(new List<string> { "MemberTestName" });
            member.Execute();

            IList<string> commandParameters = new List<string> { "1", "MemberTestName", "story" };
            //var expected = ;          
            var sut = new AssignWorkItem(commandParameters);

            Assert.IsNull(Database.Instance.Story[0].Assignee);
            sut.Execute();

            Assert.AreEqual(Database.Instance.Members[0].Name, Database.Instance.Story[0].Assignee.Name);
        }

        [TestMethod]
        public void Throw_Exception_IfNonIntIndex()
        {
            BugStatus bugStatus = BugStatus.Active;
            Severity severity = Severity.Critical;
            Priority priority = Priority.High;

            var bug = new Bug("test byg001", "description of test bug001", bugStatus, severity, priority, "steps to reproduce test bug");
            var member = new Member("MemberTestName");

            database.Bug.Add(bug);
            database.Members.Add(member);

            IList<string> commandParameters = new List<string> { "abv", "MemberTestName", "bug" };
            var sut = new AssignWorkItem(commandParameters);

            var exception = Assert.ThrowsException<ArgumentException>(() => sut.Execute());
            Assert.IsTrue(exception.Message.Contains("Failed to parse \"Add Comment to Work Item\" command parameters."));
        }

        [TestMethod]
        public void Throw_Exception_IfOutOfRangeIndex()
        {
            BugStatus bugStatus = BugStatus.Active;
            Severity severity = Severity.Critical;
            Priority priority = Priority.High;

            var bug = new Bug("test byg001", "description of test bug001", bugStatus, severity, priority, "steps to reproduce test bug");
            var member = new Member("MemberTestName");

            database.Bug.Add(bug);
            database.Members.Add(member);

            IList<string> commandParameters = new List<string> { "0", "MemberTestName", "bug" };
            var sut = new AssignWorkItem(commandParameters);

            var exception = Assert.ThrowsException<ArgumentException>(() => sut.Execute());
            Assert.IsTrue(exception.Message.Contains("Failed to parse \"Add Comment to Work Item\" command parameters."));
        }

        [TestMethod]
        public void Throw_Exception_IncorectWorkitemType()
        {
            BugStatus bugStatus = BugStatus.Active;
            Severity severity = Severity.Critical;
            Priority priority = Priority.High;

            var bug = new Bug("test byg001", "description of test bug001", bugStatus, severity, priority, "steps to reproduce test bug");
            var member = new Member("MemberTestName");

            database.Bug.Add(bug);
            database.Members.Add(member);

            IList<string> commandParameters = new List<string> { "1", "MemberTestName", "incorectTupe" };
            var sut = new AssignWorkItem(commandParameters);

            var exception = Assert.ThrowsException<ArgumentException>(() => sut.Execute());
            Assert.IsTrue(exception.Message.Contains("Failed to parse \"Add Comment to Work Item\" command parameters."));
        }
    }
}