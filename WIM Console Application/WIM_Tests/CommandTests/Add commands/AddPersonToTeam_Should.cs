﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using WIM.Commands.CommandClasses;

namespace WIM_Tests.CommandTests.Add_commands
{
    [TestClass]
    public class AddPersonToTeam_Should :BaseTestClass
    {
        [TestMethod]
        public void Correctly_AddPersonToDatabase()
        {
            //Arrange
            string nameOfPerson = "PersonTestname";
            string teamToAddTo = "TeamName";

            var person = new CreateNewPersonCommand(new List<string> { nameOfPerson });
            person.Execute();
            var team = new CreateNewTeamCommand(new List<string> { teamToAddTo });
            team.Execute();
            string expected = $"New member:{nameOfPerson} was  successfully added to Team:{teamToAddTo}.";

            //Act
            var cut = new  AddPersonToTeamCommand(new List<string> { nameOfPerson, teamToAddTo });

            //Assert
            Assert.AreEqual(expected, cut.Execute());            
        }
    }
}
