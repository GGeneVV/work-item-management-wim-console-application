﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using WIM.Commands.CommandClasses;
using WIM.Core;
using WIM.Models.Enums;
using WIM.Models.Units;

namespace WIM_Tests.CommandTests
{
    [TestClass]
   public class Create_Bug_Should:BaseTestClass
    {
        [TestMethod]
        public void Assign_Correct_Values()
        {


            Bug bug = new Bug("BugTitleAtLeast10", "Bug Description", BugStatus.Active, Severity.Minor,
                Priority.Low, stepsToReproduce: "Debug and find out whats happening");

            Assert.AreEqual(bug.Title, "BugTitleAtLeast10");
            Assert.AreEqual(bug.Description, "Bug Description");
            Assert.AreEqual(bug.BugStatus, BugStatus.Active);
            Assert.AreEqual(bug.Priority, Priority.Low);
            Assert.AreEqual(bug.StepsToReproduce, "Debug and find out whats happening");

        }
        [TestMethod]
        public void Throw_Exception_At_Title()
        {


            var exception = Assert.ThrowsException<Exception>(()=>new Bug("Bug", "Bug Description", BugStatus.Active, Severity.Minor,
                Priority.Low, stepsToReproduce: "Debug and find out whats happening"));

           

            Assert.IsTrue(exception.Message.Contains("Title should be between 10 and 50 symbols"));

            //"Bug with name {title} already exist"
        }

        //[testmethod]
        //public void assign_unique_title_values()
        //{


        //    bug bug = new bug("bugtitleatleast10", "bug description", bugstatus.active, severity.minor,
        //        priority.low, stepstoreproduce: "debug and find out whats happening");

        //    bug bug2 = new bug("bugtitleatleast10", "bug description", bugstatus.active, severity.minor,
        //        priority.low, stepstoreproduce: "debug and find out whats happening");

           


        //    assert.istrue(bug2.contains("bug with name bugtitleatleast10 already exist"));
        //}
    }
}
