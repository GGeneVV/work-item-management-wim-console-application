﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using WIM.Commands.CommandClasses;
using WIM.Models.Enums;
using WIM.Models.Units;

namespace WIM_Tests.CommandTests.FilterCommands
{
    [TestClass]
    public class Filter_By_Assignee_Should : BaseTestClass
    {
        [TestMethod]

        public void Return_All_Bugs_With_Correct_Print()
        {
            
            Bug bug = new Bug("BugTitleAtLeast10", "Bug Description", BugStatus.Active, Severity.Minor, 
                Priority.Low, stepsToReproduce: "Debug and find out whats happening");

            Member member = new Member("Angelo");

            database.Bug.Add(bug);
            database.Members.Add(member);

            bug.Assignee = member;
          

            List<string> command = new List<string> { "Angelo" };

            var filterBy = new FilterByAssigneeCommand(command);
            
            var output = filterBy.Execute();

            Assert.IsTrue(output.Contains("BugTitleAtLeast10"));
            Assert.IsTrue(output.Contains("Bug Description"));
            Assert.IsTrue(output.Contains("Angelo"));




        }
        [TestMethod]
        public void When_No_Assignee_With_That_Name()
        {
            var bug = new Bug("BugTitleAtLeast10", "Bug Description", BugStatus.Active, Severity.Minor, Priority.Low, stepsToReproduce: "Debug and find out whats happening");
            
            database.Bug.Add(bug);
            
            List<string> commandParam = new List<string> { "Angelo" };
                   
            var filterBy = new FilterByAssigneeCommand(commandParam);
            
            filterBy.Execute();

            var output = filterBy.Execute();

            Assert.IsTrue(output.Contains("There is no member with name Angelo"));
        }

       

       
    }
    
}
