﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using WIM.Commands.CommandClasses;
using WIM.Models.Enums;
using WIM.Models.Units;


namespace WIM_Tests.CommandTests.FilterCommands
{
    [TestClass]
    public class Filter_By_Assignee_And_Status_Should : BaseTestClass
    {

        [TestMethod]

        public void Return_All_Bugs_With_Given_Status_Print()
        {

            Bug bug = new Bug("BugTitleAtLeast10", "Bug Description", BugStatus.Active, Severity.Minor,
                Priority.Low, stepsToReproduce: "Debug and find out whats happening");
            Bug bug2 = new Bug("BugTitleAtLeast11", "Bug Description", BugStatus.Fixed, Severity.Minor,
                Priority.Low, stepsToReproduce: "Debug and find out whats happening");

            Member member = new Member("Angelo");


            database.Bug.Add(bug);
            database.Bug.Add(bug2);
            database.Members.Add(member);

            bug.Assignee = member;
            bug2.Assignee = member;

            List<string> command = new List<string> { "Fixed" , "Angelo"};

            var filterBy = new FilterByStatusAndAssigneeCommand(command);

            var output = filterBy.Execute();

           // Output contains only the bugs/Stories Assigned to And Having the specified status...

            Assert.IsTrue(output.Contains("BugTitleAtLeast11"));
            Assert.IsTrue(output.Contains("Fixed"));
            Assert.IsFalse(output.Contains("BugTitleAtLeast10"));
            Assert.IsFalse(output.Contains("Active"));


        }
    }
}
