# Work Item Management WIM Console Application

**Design and implement a Work Item Management (WIM) Console Application.**


**Development team board with outstanding tasks can be found at : [Our Board](https://trello.com/b/GHuu860j)**

Functional Requirements
Application should support multiple teams. Each team has name, members, and boards.
Member has name, list of work items and activity history.
- [x] -  Name should be unique in the application
- [x] -  Name is a string between 5 and 15 symbols.


Board has name, list of work items and activity history.
- [x] -  Name should be unique in the team
- [x] -  Name is a string between 5 and 10 symbols.


There are 3 types of work items: bug, story, and feedback.
Bug
Bug has ID, title, description, steps to reproduce, priority, severity, status, assignee, comments, and
history.
- [x] Title is a string between 10 and 50 symbols.
- [x] -  Description is a string between 10 and 500 symbols.
- [x] -  Steps to reproduce is a list of strings.
- [x] -  Priority is one of the following: High, Medium, Low
- [x] -  Severity is one of the following: Critical, Major, Minor
- [x] -  Status is one of the following: Active, Fixed
- [x] -  Assignee is a member from the team.
- [x] -  Comments is a list of comments (string messages with author).
- [x] -  History is a list of all changes (string messages) that were done to the bug.


Story
Story has ID, title, description, priority, size, status, assignee, comments, and history.
- [x] -  Title is a string between 10 and 50 symbols.
- [x] -  Description is a string between 10 and 500 symbols.
- [x] -  Priority is one of the following: High, Medium, Low
- [x] -  Size is one of the following: Large, Medium, Small
- [x] -  Status is one of the following: NotDone, InProgress, Done
- [x] -  Assignee is a member from the team.
- [x] -  Comments is a list of comments (string messages with author).
- [x] -  History is a list of all changes (string messages) that were done to the story.


Feedback
Feedback has ID, title, description, rating, status, comments, and history.
- [x] -  Title is a string between 10 and 50 symbols.
- [x] -  Description is a string between 10 and 500 symbols.
- [x] -  Rating is an integer.
- [x] -  Status is one of the following: New, Unscheduled, Scheduled, Done
- [x] -  Comments is a list of comments (string messages with author).
- [x] -  History is a list of all changes (string messages) that were done to the feedback.


Note: IDs of work items should be unique in the application i.e. if we have a bug with ID X then
we cannot have Story of Feedback with ID X.
Operations
Application should support the following operations:
- [x] -  Create a new person
- [x] -  Show all people
- [x] -  Show person's activity
- [x] -  Create a new team
- [x] -  Show all teams
- [x] -  Show team's activity
- [x] -  Add person to team
- [x] -  Show all team members
- [x] -  Create a new board in a team
- [x] -  Show all team boards
- [x] -  Show board's activity
- [x] -  Create a new Bug/Story/Feedback in a board
- [x] -  Change Priority/Severity/Status of a bug
- [x] -  Change Priority/Size/Status of a story
- [x] -  Change Rating/Status of a feedback
- [x] -  Assign/Unassign work item to a person
- [x] -  Add comment to a work item
- [x] -  List work items with options:
- [x] -  List all
- [x] -  Filter bugs/stories/feedback only
- [x] -  Filter by status and/or assignee
- [x] -  Sort by title/priority/severity/size/rating 


**General Requirements**
**Follow the OOP best practices:****

 - [ ] -  Proper use data encapsulation
- [ ]  -  Proper use of inheritance and polymorphism
- [ ]  -  Proper use of interfaces and abstract classes
- [ ]  -  Proper use of static members
- [ ]  -  Proper use enumerations
- [ ]  -  Follow the principles of strong cohesion and loose coupling


- [ ] -  Use LINQ
- [ ] -  Implement proper user input validation and display meaningful user messages
- [ ] -  Implement proper exception handling
- [ ] -  Cover functionality with unit tests (80% code coverage)
- [ ] -  Use Git to keep your source code and for team collaboration


**Teamwork Requirements**

 **Create a Trello board with the following data, fill it and keep it updated:**
  **Features:**
- [ ]  *Name* - the name of the Trello card would be the given feature / task that needs to
- [ ] be done
- [ ]  *Size* - what is the size of the feature in terms of programming effort i.e.
- [ ] Large/Medium/Small
- [ ]  *Priority* - what is the importance of the feature i.e. Must/Should/Could
- [ ]  *Owner* - who is responsible for the successful completion of the given feature / task
- [ ]  *Bugs* - any issues / problems of the software
- [ ]  *Ideas* - any additional ideas for new features or improvements in the project 



**Teamwork defense**

*Prepare a list of commands to demonstrate how the program works.*

*Mock Defence*